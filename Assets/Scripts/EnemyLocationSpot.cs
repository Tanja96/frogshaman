﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyLocationType
{
    stand,
    gather,
    walk
}

public class EnemyLocationSpot : MonoBehaviour {

    public EnemyLocationType type;
    public Transform[] wayPoints;
}
