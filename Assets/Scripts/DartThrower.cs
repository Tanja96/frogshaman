﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DartThrower : MonoBehaviour
{
    public GameObject dart;

    private Coroutine throwDart;
    private int characterCount = 0;

    private IEnumerator ThrowDart()
    {
        WaitForSeconds delay = new WaitForSeconds(0.5f);
        while(true)
        {
            Vector3 rot = transform.rotation.eulerAngles + new Vector3(-90,0,0);
            GameObject obj = Instantiate(dart, transform.position + transform.forward, Quaternion.Euler(rot));
            obj.GetComponent<Rigidbody>().AddForce(transform.forward * 20, ForceMode.Impulse);
            Destroy(obj, 2f);
            yield return delay;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player") || other.gameObject.layer == LayerMask.NameToLayer("Enemy"))
        {
            if(characterCount == 0)
            {
                throwDart = StartCoroutine(ThrowDart());
            }
            characterCount++;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player") || other.gameObject.layer == LayerMask.NameToLayer("Enemy"))
        {
            characterCount--;
            if(characterCount == 0)
            {
                StopCoroutine(throwDart);
            }
        }
    }
}
