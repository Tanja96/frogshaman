﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    public Transform frog;
    public Transform cameraObject;
    public float cameraSpeedHorizontal;
    public float cameraSpeedVertical;
    public int horizontalDir;
    public int verticalDir;
    public float min;
    public float max;

    public float multiplier;
    public float xMultiplier;
    public float groundHeight;
    public float angleScaler;
    public float maxCameraDistance;
    public float minCameraDistance;
    public Vector2 origin;
    public float lerpSpeed;
    public Transform cameraTarget;
    public LayerMask hitLayers;
    public MeshRenderer frogMesh;
    public List<Transform> enemies = new List<Transform>();
    public float smoothTime;

    private Vector3 lastPos;
    private float horizontal = 0;
    private float vertical = 0;
    private float cameraDistance;
    private float timer = 0;
    private bool followTarget = false;
    private Transform target;
    private Vector3 locPos;
    private FrogMovement frogMovement;
    private bool targetFound = false;
    private bool findingTarget = false;
    private float targettingDistance;
    private bool targetReached = false;

    private void Start()
    {
        lastPos = frog.position;
        cameraDistance = Vector3.Distance(cameraObject.position, transform.position);
        frogMovement = FindObjectOfType<FrogMovement>();
        targettingDistance = FindObjectOfType<TargettingCone>().GetComponent<MeshCollider>().bounds.extents.z * 2;
    }

    private void FixedUpdate()
    {
        //Moving middlePoint
        transform.position += (frog.position - lastPos);

        if(followTarget)
        {
            if(target != null && Vector3.Distance(target.position, frog.position) < targettingDistance)
            {
                if(targetReached)
                {
                    transform.LookAt(target);
                }
                else
                {
                    SmoothTarget();
                }
            }
            else if(!findingTarget)
            {
                StartCoroutine(TargetFindingTimer());
                findingTarget = true;
            }
            CheckTargetChanging();
        }
        else //Normal Camera movement
        {
            if (horizontal != 0 || vertical != 0)
            {
                RotateCamera();
                timer = 0;
            }
            else if (frog.position != lastPos)
            {
                timer += Time.deltaTime;
                if(timer > 1f)
                {
                    AutomatedMovement();
                }
            }
        }
        CheckForObstacles();
        lastPos = frog.position;
    }

    private void AutomatedMovement()
    {
        Vector3 movementVec = frog.forward;
        movementVec = new Vector3(movementVec.x, 0, movementVec.z);
        Vector3 cameraVec = cameraObject.position - frog.position;
        cameraVec = new Vector3(cameraVec.x, 0, cameraVec.z);
        float angle = Vector3.Angle(movementVec, cameraVec);
        if (angle > 20)
        {
            transform.eulerAngles = new Vector3(
                Mathf.LerpAngle(transform.eulerAngles.x, cameraTarget.eulerAngles.x, Time.deltaTime /** (1 + (1 - angle/180))*/),
                Mathf.LerpAngle(transform.eulerAngles.y, cameraTarget.eulerAngles.y, Time.deltaTime /** (1 + (1 - angle / 180))*/),
                0);
        }
    }

    private void RotateCamera()
    {
        //Rotating middlePoint
        transform.Rotate(Vector3.up * horizontalDir * horizontal * cameraSpeedHorizontal, Space.World);
        transform.Rotate(Vector3.right * verticalDir * vertical * cameraSpeedVertical);

        //Checking that rotation in bounds
        if (transform.eulerAngles.x > 180 && transform.eulerAngles.x < min)
        {
            transform.eulerAngles = new Vector3(min, transform.eulerAngles.y, 0);
        }
        else if (transform.eulerAngles.x < 180 && transform.eulerAngles.x >= max)
        {
            transform.eulerAngles = new Vector3(max, transform.eulerAngles.y, 0);
        }
    }

    private void CalculateCameraDistance()
    {
        float x;
        if (cameraObject.eulerAngles.x > 180)
        {
            x = (cameraObject.eulerAngles.x - 360 + (360 - min)) / angleScaler;
        }
        else
        {
            x = (cameraObject.eulerAngles.x + (360 - min)) / angleScaler;
        }
        float y = multiplier * x * x * x - xMultiplier * x + groundHeight;

        Vector2 point = new Vector2(x, y);

        cameraDistance = Vector2.Distance(origin, point);
        if (cameraDistance > maxCameraDistance)
        {
            cameraDistance = maxCameraDistance;
        }
        else if (cameraDistance < minCameraDistance)
        {
            cameraDistance = minCameraDistance;
        }

        cameraObject.position = transform.position + (cameraObject.position - transform.position).normalized * cameraDistance;
    }

    private void CheckForObstacles()
    {
        Vector3 dir = cameraObject.position - transform.position;
        RaycastHit hit;
        if(Physics.Raycast(transform.position, dir, out hit, cameraDistance, hitLayers) && !hit.collider.isTrigger)
        {
            cameraObject.position = hit.point - dir.normalized * 0.25f;
        }
        else if (followTarget)
        {
            cameraObject.localPosition = locPos;
        }
        else
        {
            CalculateCameraDistance();
        }
        if((cameraObject.position - transform.position).magnitude < 1.5f)
        {
            frogMesh.enabled = false;
        }
        else
        {
            frogMesh.enabled = true;
        }
    }

    public void SetMovementAxis(float h, float v)
    {
        horizontal = h;
        vertical = v;
        if(horizontal == 0)
        {
            targetFound = false;
        }
    }

    public void SetTarget(bool value)
    {
        followTarget = value;
        if(followTarget)
        {
            FindTarget();
        }
        else
        {
            frogMovement.targetting = false;
            target = null;
        }
    }

    private void FindTarget()
    {
        float bestDistance = 10000;
        bool targetFound = false;
        foreach(Transform enemy in enemies)
        {
            if(enemy != null)
            {
                float dist = Vector3.Distance(transform.position, enemy.position);
                if(dist < bestDistance)
                {
                    bestDistance = dist;
                    TargetSetter(enemy);
                    targetFound = true;
                }
            }
        }
        if(!targetFound)
        {
            target = null;
            frog.transform.rotation = Quaternion.Euler(frog.eulerAngles.x, transform.eulerAngles.y, 0);
        }
        Quaternion lastRot = transform.rotation;
        transform.rotation = cameraTarget.rotation;
        CalculateCameraDistance();
        locPos = cameraObject.localPosition;
        transform.rotation = lastRot;
        CalculateCameraDistance();
        StartCoroutine(ToCameraTarget());
        frogMovement.targetting = true;
    }

    private void CheckTargetChanging()
    {
        if(!targetFound && horizontal != 0)
        {
            float bestDistance = 10000;
            if (enemies.Count > 0)
            {
                foreach (Transform enemy in enemies)
                {
                    if(enemy != null)
                    {
                        //d=(x−x1)(y2−y1)−(y−y1)(x2−x1) from the internet, it tells on which side of the player the enemy is
                        float d = (enemy.position.x - transform.position.x) * transform.forward.z - (enemy.position.z - transform.position.z) * transform.forward.x;
                        if (enemy != target && ((horizontal < 0 && d < 0) || (horizontal > 0 && d > 0)))
                        {
                            float dist = Vector3.Distance(transform.position, enemy.position);
                            if (dist < bestDistance)
                            {
                                TargetSetter(enemy);
                                bestDistance = dist;
                                targetFound = true;
                            }
                        }
                    }
                }
            }
        }
    }

    private IEnumerator TargetFindingTimer()
    {
        do
        {
            float bestDistance = 10000;
            if (enemies.Count > 0)
            {
                foreach (Transform enemy in enemies)
                {
                    if (enemy != null)
                    {
                        float dist = Vector3.Distance(transform.position, enemy.position);
                        if (dist < bestDistance)
                        {
                            TargetSetter(enemy);
                            bestDistance = dist;
                            break;
                        }
                    }
                }
            }
            yield return new WaitForSeconds(0.5f);
        } while (target == null && followTarget);
        findingTarget = false;
    }

    private void TargetSetter(Transform enemy)
    {
        target = enemy;
        var targetRotation = Quaternion.LookRotation(target.transform.position - transform.position);
        frog.transform.rotation = Quaternion.Euler(frog.eulerAngles.x, targetRotation.eulerAngles.y, 0);
        targetReached = false;
    }

    private IEnumerator ToCameraTarget()
    {
        float movingTimer = 0;
        while (movingTimer < 1 && followTarget)
        {
            if(target != null)
            {
                var targetRotation = Quaternion.LookRotation(target.transform.position - transform.position);

                Quaternion combinedRotation = Quaternion.Euler(targetRotation.eulerAngles.x, cameraObject.eulerAngles.y, 0);

                transform.rotation = Quaternion.Slerp(transform.rotation, combinedRotation, movingTimer);
            }
            else
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, cameraTarget.rotation, movingTimer);
            }
            CalculateCameraDistance();
            movingTimer += Time.deltaTime * smoothTime * 0.1f;
            yield return null;
        }
    }

    private void SmoothTarget()
    {
        var targetRotation = Quaternion.LookRotation(target.transform.position - transform.position);

        // Smoothly rotate towards the target point.
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, smoothTime * Time.deltaTime);
        if (transform.rotation == targetRotation)
        {
            targetReached = true;
        }
    }

    public Transform GetTarget()
    {
        return target;
    }
}
