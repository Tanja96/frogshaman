﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemySpawner : MonoBehaviour, IDestroyable
{
    public int health;
    public int maxSmallEnemies;
    public int maxMiddleEnemies;
    public int maxBigEnemies;
    public float spawnDelay;
    public Transform spawnPoint;
    public GameObject smallEnemy;
    public GameObject middleEnemy;
    public GameObject bigEnemy;
    public Image healthBar;
    public Transform healthBarCanvas;
    public Transform cameraObj;
    public EnemyLocationSpot[] locationSpots;
    private Dictionary<EnemyLocationSpot, bool> spots = new Dictionary<EnemyLocationSpot, bool>();

    public int currentSmallEnemies;
    public int currentMiddleEnemies;
    public int currentBigEnemies;

    private int currentHealth;

    private void Awake()
    {
        foreach(EnemyLocationSpot spot in locationSpots)
        {
            spots.Add(spot, true);
        }
    }

    private void Start()
    {
        currentHealth = health;
        StartCoroutine(EnemySpawn());
    }

    private void Update()
    {
        healthBarCanvas.LookAt(new Vector3(cameraObj.position.x, healthBarCanvas.position.y, cameraObj.position.z));
    }

    IEnumerator EnemySpawn()
    {
        yield return new WaitForSeconds(spawnDelay);
        if (currentSmallEnemies < maxSmallEnemies || currentMiddleEnemies < maxMiddleEnemies || currentBigEnemies < maxBigEnemies)
        {
            float value = Random.value;
            List<int> candidates = new List<int>();
            if (currentSmallEnemies < maxSmallEnemies)
            {
                candidates.Add(0);
            }
            if (currentMiddleEnemies < maxMiddleEnemies)
            {
                candidates.Add(1);
            }
            if (currentBigEnemies < maxBigEnemies)
            {
                candidates.Add(2);
            }
            if (candidates.Count > 0)
            {
                if (value <= 0.6f)
                {
                    InstantiateEnemy(candidates, 0);
                }
                else if (value < 0.88f && candidates.Count >= 2)
                {
                    InstantiateEnemy(candidates, 1);
                }
                else if (value >= 0.88f && candidates.Count == 3)
                {
                    InstantiateEnemy(candidates, 2);
                }
                else
                {
                    InstantiateEnemy(candidates, 0);
                }
            }
        }
        StartCoroutine(EnemySpawn());
    }

    private void InstantiateEnemy(List<int> candidates, int index)
    {
        if (candidates[index] == 0)
        {
            GameObject spawned = Instantiate(smallEnemy);
            SmallEnemy script = spawned.GetComponent<SmallEnemy>();
            script.spawner = this;
            script.homeSpot = GiveHomeSpot();
            script.SetStartLocation(spawnPoint.position);
            ++currentSmallEnemies;
        }
        else if (candidates[index] == 1)
        {
            GameObject spawned = Instantiate(middleEnemy);
            MiddleEnemy script = spawned.GetComponent<MiddleEnemy>();
            script.spawner = this;
            script.homeSpot = GiveHomeSpot();
            script.SetStartLocation(spawnPoint.position);
            ++currentMiddleEnemies;
        }
        else if (candidates[index] == 2)
        {
            GameObject spawned = Instantiate(bigEnemy);
            spawned.transform.position = spawnPoint.position;
            ++currentBigEnemies;
        }
    }

    private EnemyLocationSpot GiveHomeSpot()
    {
        foreach(EnemyLocationSpot spot in spots.Keys)
        {
            if(spots[spot])
            {
                return spot;
            }
        }
        return null;
    }

    public void GetDamage(int amount)
    {
        currentHealth -= amount;
        healthBar.fillAmount = (float)currentHealth / (float)health;
        if (currentHealth <= 0)
        {
            Destroy(gameObject);
        }
    }

    public void TakeSpot(EnemyLocationSpot spot)
    {
        spots[spot] = false;
    }
    
    public void FreeSpot(EnemyLocationSpot spot)
    {
        spots[spot] = true;
    }
}
