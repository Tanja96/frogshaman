﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flames : MonoBehaviour
{
    private float value;
    private IDestroyable destroyable;
    private float timer = 0;
    public bool smokePlaying = false;
    private int curHits;
    private float lastFreeze;
    private float targetSize;
    private Vector3 growDir;
    private Vector3 targetPos;
    private int moveDir;
    private float targetEmission;
    private ParticleSystem.EmissionModule emission;
    private ParticleSystem.ShapeModule shape;
    private BurnableObject burnableObject;

    public int spreadAmount;
    public ParticleSystem smoke;
    public ParticleSystem flames;
    public int hitsBeforeExtinguishing;
    public float dyingTime;
    public int damage;

    private void Start()
    {
        value = Random.Range(0.5f, 1f);
        if(spreadAmount > 0)
        {
            StartCoroutine(CheckBurnables());
        }
        StartCoroutine(StopTimer());
        destroyable = transform.root.GetComponent<IDestroyable>();
        burnableObject = transform.root.GetComponent<BurnableObject>();
        if(burnableObject != null)
        {
            Vector3 size = transform.root.GetComponent<MeshRenderer>().bounds.size;
            if (size.x > size.y && size.x > size.z)
            {
                growDir = new Vector3(1, 0, 0);
            }
            else if(size.y > size.x && size.y > size.x)
            {
                growDir = new Vector3(0, 1, 0);
            }
            else
            {
                growDir = new Vector3(0, 0, 1);
            }
            shape = flames.shape;
            shape.shapeType = ParticleSystemShapeType.Rectangle;
            targetSize = shape.scale.x + size.x;
            targetPos = new Vector3(transform.root.position.x, transform.position.y, transform.position.z);
            if(transform.position.x < targetPos.x)
            {
                moveDir = 1;
            }
            else
            {
                moveDir = -1;
            }
            emission = flames.emission;
            targetEmission = emission.rateOverTimeMultiplier * size.x;
        }
    }

    private void Update()
    {
        if(destroyable != null)
        {
            timer += Time.deltaTime;
            if(timer > 1)
            {
                destroyable.GetDamage(damage);
                timer = 0;
            }
        }
        if(flames != null && flames.particleCount == 0)
        {
            StopAllCoroutines();
            Destroy(this.gameObject);
        }
        else if(burnableObject != null)
        {
            if(shape.scale.x < targetSize)
            {
                shape.scale += growDir * Time.deltaTime;
            }
            if(Vector3.Distance(transform.position, targetPos) > 0.1f)
            {
                transform.position += new Vector3(Time.deltaTime * 0.5f * moveDir, 0,0);
            }
            if(emission.rateOverTimeMultiplier < targetEmission)
            {
                emission.rateOverTimeMultiplier += Time.deltaTime * 10;
            }
        }
    }

    private IEnumerator CheckBurnables()
    {
        yield return new WaitForSeconds(value);
        Collider[] hits = Physics.OverlapBox(transform.position, new Vector3(1, 1, 1));
        if (hits != null) 
        {
            foreach(Collider hit in hits)
            {
                if(hit.transform.GetComponent<BurnCounter>() != null)
                {
                    GameObject obj = Instantiate(gameObject, hit.ClosestPoint(transform.position), gameObject.transform.rotation);
                    Vector3 scale = transform.localScale;
                    obj.transform.parent = hit.transform;
                    obj.transform.localScale = scale;
                    obj.GetComponent<Flames>().spreadAmount = spreadAmount - 1;
                    Destroy(obj, 3);
                }
            }
        }
    }

    private IEnumerator StopTimer()
    {
        yield return new WaitForSeconds(dyingTime);
        flames.Stop();
        smoke.Stop();
    }

    public void Extinguish()
    {
        smoke.Stop();
        flames.Stop();
        smokePlaying = false;
    }
}
