﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum FlyElement : int {fire, water, ice, air, spark, earth}

public class Flies : MonoBehaviour {
    public FlyElement element; //
    public GameObject spawner;
	// Use this for initialization

	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnDestroy()
    {
        if (spawner != null)
            --spawner.GetComponent<FlySpawner>().flyAmount;
    }

}
