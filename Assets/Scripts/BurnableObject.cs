﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurnableObject : MonoBehaviour
{
    public float burnTime;
    public GameObject SparkParticles;

    private MaterialPropertyBlock props;
    private MeshRenderer meshRenderer;
    private float burnAmount = 0;
    private float emissionAmount = 0;
    private float cutoff = 0;
    private bool burningStarted = false;
    private bool destroyStarted = false;
    private float curBurnTime = 0;
    private Coroutine destroyer;

    private void Start()
    {
        props = new MaterialPropertyBlock();
        meshRenderer = GetComponent<MeshRenderer>();
    }

    private void Update()
    {
        meshRenderer.GetPropertyBlock(props);
        if(burningStarted)
        {
            curBurnTime += Time.deltaTime;
            burnAmount += Time.deltaTime * 2;
            burnAmount = Mathf.Clamp01(burnAmount);
            if(burnAmount > 0.5f)
            {
                emissionAmount += Time.deltaTime;
                emissionAmount = Mathf.Clamp01(emissionAmount);
                props.SetFloat("_EmissionAmount", emissionAmount);
            }
            props.SetFloat("_BurnAmount", burnAmount);
            meshRenderer.SetPropertyBlock(props);
        }
        else if(burnAmount > 0.5f)
        {
            emissionAmount -= Time.deltaTime;
            emissionAmount = Mathf.Clamp01(emissionAmount);
            props.SetFloat("_EmissionAmount", emissionAmount);
            meshRenderer.SetPropertyBlock(props);
        }
        if(destroyStarted)
        {
            cutoff += Time.deltaTime;
            cutoff = Mathf.Clamp01(cutoff);
            props.SetFloat("_Cutoff", cutoff);
            meshRenderer.SetPropertyBlock(props);
            if(cutoff >= 1)
            {
                Destroy(this.gameObject);
            }
        }
    }

    public void Burn(GameObject flame)
    {
        if(burningStarted == false)
        {
            burningStarted = true;
            flame.GetComponent<Flames>().dyingTime = burnTime - curBurnTime;
            destroyer = StartCoroutine(Destroyer());
        }
    }

    public void StopBurn()
    {
        if(burningStarted && !destroyStarted)
        {
            burningStarted = false;
            StopCoroutine(destroyer);
        }
    }

    private IEnumerator Destroyer()
    {
        yield return new WaitForSeconds(burnTime - curBurnTime);
        destroyStarted = true;
        Instantiate(SparkParticles, transform.position, Quaternion.identity);
    }
}
