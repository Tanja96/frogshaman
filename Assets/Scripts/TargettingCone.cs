﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargettingCone : MonoBehaviour {

    private CameraMovement movement;

    private void Start()
    {
        movement = GetComponentInParent<CameraMovement>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Enemy"))
        {
            movement.enemies.Add(other.transform);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Enemy"))
        {
            movement.enemies.Remove(other.transform);
        }
    }
}
