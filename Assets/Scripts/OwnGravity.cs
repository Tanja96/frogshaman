﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OwnGravity : MonoBehaviour {

    private Rigidbody rigi;
    private float globalGravity = -9.81f;
    private float gravityScale = 1.0f;

	void Start ()
    {
        rigi = GetComponent<Rigidbody>();
	}
	
	void FixedUpdate ()
    {
        Vector3 gravity = globalGravity * gravityScale * Vector3.up;
        rigi.AddForce(gravity, ForceMode.Acceleration);
    }

    public void ChangeGravityScale(int gravity)
    {
        gravityScale = gravity;
    }
}
