﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Staff : MonoBehaviour
{
    public int damage;

    private void OnTriggerEnter(Collider collider)
    {
        IDestroyable destroyable = collider.GetComponent<IDestroyable>();
        if (destroyable != null)
        {
            destroyable.GetDamage(damage);
        }
    }
}
