﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Data;
using System;
using Mono.Data.Sqlite;

public class UIManager : MonoBehaviour {
    
    public Text healthText;
    public Image currentSpell;
    public GameObject[] elements = new GameObject[5];
    public GameObject elementSelectionPanel;
    public Image element1;
    public Image element2;

    private int elementNumber;
    private FlyElement?[] elementSlots = new FlyElement?[2];
    private bool elementChanged = false;
    private int index;
    private Frog frog;

    private void Start()
    {
        frog = FindObjectOfType<Frog>();
    }
    
    public void SetHealth(int health)
    {
        healthText.text = "Health: " + health;
    }

    public void SetCurrentSpell(Sprite sprite)
    {
        currentSpell.sprite = sprite;
    }

    string CheckElements()
    {
        string haku = "";
        int cFire = 0;
        int cWater = 0;
        int cIce = 0;
        int cAir = 0;
        int cSpark = 0;
        int cEarth = 0;
        List<FlyElement> spellElements = frog.GetComponent<Frog>().spellElements;
        if (spellElements.Count > 0)
        {
            for (int i = 0; i < spellElements.Count; ++i)
            {
                if (spellElements[i] == FlyElement.fire)
                {
                    cFire += 1;
                }
                else if (spellElements[i] == FlyElement.water)
                {
                    cWater += 1;
                }
                else if (spellElements[i] == FlyElement.ice)
                {
                    cIce += 1;
                }
                else if (spellElements[i] == FlyElement.air)
                {
                    cAir += 1;
                }
                else if (spellElements[i] == FlyElement.spark)
                {
                    cSpark += 1;
                }
                else if (spellElements[i] == FlyElement.earth)
                {
                    cEarth += 1;
                }
            }
            haku = " Fire = " + cFire + " AND" + " Ice = " + cIce + " AND" + " Air = " + cAir + " AND" + " Spark = " + cSpark + " AND" + " Earth = " + cEarth;
        }
        else
        {
            haku = " Name = 'None'";
        }

        return haku;
    }

    public void SetSelectPanel(bool press, int number)
    {
        if(press)
        {
            elementNumber = number;
            elements[index].transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
        }
        else
        {
            if(!elementChanged)
            {
                if (elementNumber == 1)
                {
                    elementSlots[0] = null;
                    element1.sprite = null;
                }
                else
                {
                    elementSlots[1] = null;
                    element2.sprite = null;
                }
            }
            frog.spellElements.Clear();
            if(elementSlots[0].HasValue)
            {
                frog.spellElements.Add(elementSlots[0].Value);
            }
            if(elementSlots[1].HasValue)
            {
                frog.spellElements.Add(elementSlots[1].Value);
            }
            string conn = "URI=file:" + Application.streamingAssetsPath + "/FrogshamanSpells.db"; //Path to database.
            IDbConnection dbconn;
            dbconn = (IDbConnection)new SqliteConnection(conn);
            dbconn.Open(); //Open connection to the database.
            IDbCommand dbcmd = dbconn.CreateCommand();
            string sqlQuery = "SELECT Spell FROM Spells WHERE" + CheckElements();
            dbcmd.CommandText = sqlQuery;
            IDataReader reader = dbcmd.ExecuteReader();
            while (reader.Read())
            {
                string name = reader.GetString(0);
                Debug.Log("  name =" + name);
                if (name != "None")
                {
                    GameObject obj = Resources.Load<GameObject>("Spells/" + name);
                    if(obj != null) // TÄN VOI MYÖHEMMIN OTTAA POIS
                    {
                        frog.spell = obj;
                        currentSpell.sprite = obj.GetComponent<ASpell>().spellImage;
                    }
                }
                else
                {
                    frog.spell = null;
                    currentSpell.sprite = null;
                }
            }
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            dbconn.Close();
            dbconn = null;
            elementChanged = false;
        }
        elementSelectionPanel.SetActive(press);
    }

    public void ChangeSelection(float h, float v)
    {
        if ((h >= 0 && h < 0.9f) && (v >= 0 && v < 0.9f))
        {
            return;
        }
        else if(Math.Abs(h) <= 0.2f && Math.Abs(v) <= 0.2f)
        {
            elementSlots[elementNumber - 1] = null;
            elementChanged = false;
        }
        else
        {
            elementChanged = true;
            float angle = Mathf.Rad2Deg * (float)Math.Atan2(v, h);
            if(angle < 0)
            {
                angle = 180 + (180 + angle);
            }
            elements[index].transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            index = 0;
            if (angle >= 0 && angle < 72)
                index = 0;
            else if (angle >= 72 && angle < 144)
                index = 1;
            else if (angle >= 144 && angle < 216)
                index = 2;
            else if (angle >= 216 && angle < 288)
                index = 3;
            else if (angle >= 288 && angle < 360)
                index = 4;
            GameObject obj = elements[index];
            obj.transform.localScale = new Vector3(2, 2, 2);
            elementSlots[elementNumber - 1] = obj.GetComponent<Element>().element;
            if(elementNumber == 1)
            {
                element1.sprite = obj.GetComponent<Image>().sprite;
            }
            else
            {
                element2.sprite = obj.GetComponent<Image>().sprite;
            }
        }
    }
}
