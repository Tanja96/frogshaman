﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aim : MonoBehaviour
{
    public Camera mainCamera;
    public GameObject aimPrefab;
    public int maxDistance;
    public int horizontalDir;
    public int verticalDir;
    public float cameraSpeedHorizontal;
    public float cameraSpeedVertical;
    public LayerMask layers;

    private Camera aimCamera;
    private float horizontal = 0;
    private float vertical = 0;
    private bool aiming = false;
    public GameObject aimCircle;

    private void Awake()
    {
        aimCamera = GetComponent<Camera>();
        aimCamera.enabled = false;
    }

    public void FixedUpdate()
    {
        if(aiming)
        {
            transform.Rotate(Vector3.up * horizontalDir * horizontal * cameraSpeedHorizontal, Space.World);
            transform.Rotate(Vector3.right * verticalDir * vertical * cameraSpeedVertical);
            MoveAimCircle();
        }
    }

    public void StartAim()
    {
        aiming = true;
        mainCamera.enabled = false;
        aimCamera.enabled = true;
        aimCircle = Instantiate(aimPrefab);
        MoveAimCircle();
    }

    public void EndAim()
    {
        aiming = false;
        mainCamera.enabled = true;
        aimCamera.enabled = false;
        Destroy(aimCircle);
        aimCircle = null;
    }

    public void SetMovementAxis(float h, float v)
    {
        horizontal = h;
        vertical = v;
    }

    private void MoveAimCircle()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, maxDistance, layers))
        {
            aimCircle.transform.position = hit.point;
            aimCircle.transform.forward = hit.normal;
        }
        else
        {
            aimCircle.transform.position = transform.position + transform.forward * maxDistance;
            aimCircle.transform.forward = -transform.forward;
        }
    }
}
