﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricityBox : MonoBehaviour, ILightningEffect
{
    public GameObject activatableObject;
    private IActivatable activatable;

    private void Start()
    {
        activatable = activatableObject.GetComponent<IActivatable>();
    }

    public void Electrocute()
    {
        activatable.Activate();
    }
}
