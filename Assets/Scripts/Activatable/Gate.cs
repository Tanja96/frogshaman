﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour, IActivatable
{
    public Transform upTransform;
    public Transform downTransform;
    public float movingSpeed;

    private Vector3 up;
    private Vector3 down;
    private Vector3 startPos;
    private Vector3 endPos;
    private bool moving;
    private float startTime;
    private float journeyLength;

    private void Start()
    {
        up = upTransform.position;
        down = downTransform.position;
    }

    private void Update()
    {
        if(moving)
        {
            float distCovered = (Time.time - startTime) * movingSpeed;
            float fracJourney = distCovered / journeyLength;
            transform.position = Vector3.Lerp(startPos, endPos, fracJourney);
            if(transform.position == endPos)
            {
                moving = false;
            }
        }
    }

    public void Activate()
    {
        moving = true;
        startPos = transform.position;
        endPos = down;
        startTime = Time.time;
        journeyLength = Vector3.Distance(transform.position, down);
    }

    public void Deactivate()
    {
        moving = true;
        startPos = transform.position;
        endPos = up;
        startTime = Time.time;
        journeyLength = Vector3.Distance(transform.position, up);
    }
}
