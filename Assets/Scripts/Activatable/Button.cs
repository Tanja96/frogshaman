﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour
{
    public GameObject activatableObject;
    public bool isDeactivating;
    private IActivatable activatable;

    private void Start()
    {
        activatable = activatableObject.GetComponent<IActivatable>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") || other.gameObject.layer == LayerMask.NameToLayer("Enemy") || other.CompareTag("Rock"))
        {
            activatable.Activate();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if ((other.CompareTag("Player") || other.gameObject.layer == LayerMask.NameToLayer("Enemy") || other.CompareTag("Rock")) && isDeactivating)
        {
            activatable.Deactivate();
        }
    }
}
