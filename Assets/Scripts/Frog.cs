﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;


public class Frog : MonoBehaviour, IDestroyable, ISpellEffect {

    public int health;
    public bool isHidden = false;
    public int attackDamage;
    public Collider staff;
    public bool attack = false;
    public bool cast = false;
    public float timer = 0;
    public GameObject spell;
    public int spellDamage;
    public List<FlyElement> spellElements = new List<FlyElement>();

    private Animator animator;
    private FrogMovement movement;
    public GameObject currentSpell;

    private void Start()
    {
        animator = GetComponent<Animator>();
        movement = GetComponent<FrogMovement>();
    }

    private void FixedUpdate()
    {
        if(attack)
        {
            timer += Time.deltaTime;
            if(animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.9f)
            {
                staff.enabled = false;
                attack = false;
                timer = 0;
                animator.SetBool("attack", false);
            }
        }
        else if(cast)
        {
            cast = false;
            CastSpell();
        }
    }

    public void CastSpell()
    {
        if(spell != null)
        {
            currentSpell = Instantiate(spell, transform.position, transform.rotation);
            currentSpell.transform.parent = transform;
        }
    }

    public void GetDamage(int amount)
    {
        health -= amount;
        FindObjectOfType<UIManager>().SetHealth(health);
        if(health <= 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    public void SetAttack(bool press)
    {
        if(!attack)
        {
            attack = press;
            staff.enabled = true;
            animator.SetBool("attack", true);
            if (!movement.IsGrounded)
            {
                movement.fallAttack = true;
            }
        }
    }

    public void StartCast()
    {
        cast = true;
    }

    public void StopCast()
    {
        if(currentSpell != null)
        {
            currentSpell.GetComponent<ASpell>().Cast();
        }
    }

    public void Burn()
    {
        throw new System.NotImplementedException();
    }

    public void SetSpeed(bool normalSpeed)
    {
        
    }

    public void SetStopped(bool stopped)
    {
        
    }
}
