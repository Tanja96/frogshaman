﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrogMovement : MonoBehaviour {

    private Rigidbody rigi;
    private Vector3 lastPos;
    private float horizontal = 0;
    private float vertical = 0;
    private bool isGrounded = false;
    private bool jumpPressed = false;
    private bool dashPressed = false;
    private bool dashing = false;
    private int jumpCount = 0;
    private bool comingDown = false;
    public bool onTornado = false;

    public bool IsGrounded { get { return isGrounded; } }
    public Transform cameraObject;
    public LayerMask layers;
    public float speed = 0.2f;
    public float jumpForce = 5.2f;
    public float dashForce = 20;
    [Range(1,5)]
    public float fallMultiplier = 2.5f;
    [Range(1, 5)]
    public float lowJumpMultiplier = 2.0f;
    [Range(1, 20)]
    public float fallAttackMultiplier;
    public bool fallAttack = false;
    public bool targetting = false;
    public int speedMultiplier = 1;
    private Frog frog;
    private Tornado tornado;

    private void Start()
    {
        rigi = GetComponent<Rigidbody>();
        lastPos = transform.position;
        frog = FindObjectOfType<Frog>();
    }

    private void Update()
    {
        GroundCheck();
    }

    private void FixedUpdate()
    {
        #region Movement
        if (horizontal != 0 || vertical != 0)
        {
            Vector3 forward = new Vector3(cameraObject.forward.x, 0, cameraObject.forward.z).normalized;
            Vector3 right = new Vector3(cameraObject.right.x, 0, cameraObject.right.z).normalized;
            Vector3 direction = right * horizontal + forward * vertical;
            
            if(frog.attack)
            {
                speed = 0.1f * speedMultiplier;
            }
            else
            {
                speed = 0.2f * speedMultiplier;
            }
            if(!onTornado)
            {
                rigi.MovePosition(transform.position + new Vector3(direction.x, 0, direction.z) * speed);
            }

            if(targetting)
            {
                rigi.MoveRotation(Quaternion.LookRotation(new Vector3(cameraObject.forward.x, 0, cameraObject.forward.z).normalized, transform.up));
            }
            else
            {
                rigi.MoveRotation(Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z).normalized, transform.up));
            }
        }
        #endregion
        #region Jump
        if(fallAttack)
        {
            rigi.velocity += Vector3.up * Physics.gravity.y * (fallAttackMultiplier - 1) * Time.deltaTime;
            comingDown = true;
        }
        else if (!onTornado)
        {
            if (rigi.velocity.y < 0)
            {
                rigi.velocity += Vector3.up * Physics.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
                comingDown = true;
            }
            else if (rigi.velocity.y > 0 && !jumpPressed)
            {
                rigi.velocity += Vector3.up * Physics.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
            }
        }
        #endregion
        #region Dash
        if(dashPressed && isGrounded && !dashing)
        {
            StartCoroutine(Dash());
            dashing = true;
        }
        #endregion
        lastPos = transform.position;
    }

    private void GroundCheck()
    {
        if(Physics.Raycast(transform.position, Vector3.down, 1.01f, layers))
        {
            isGrounded = true;
            if(comingDown)
            {
                jumpCount = 0;
                fallAttack = false;
            }
        }
        else
        {
            isGrounded = false;
        }
    }

    private IEnumerator Dash()
    {
        WaitForSeconds delay = new WaitForSeconds(0.3f);
        if(targetting && transform.position != lastPos)
        {
            rigi.velocity = (transform.position - lastPos).normalized * dashForce;
        }
        else
        {
            rigi.velocity = transform.forward * dashForce;
        }
        yield return delay;
        rigi.velocity = new Vector3(0, 0, 0);
        yield return delay;
        dashing = false;
    }

    public void SetJump(bool press)
    {
        jumpPressed = press;
        if(jumpPressed && jumpCount < 2)
        {
            rigi.velocity = Vector3.up * jumpForce;
            jumpCount++;
            comingDown = false;
        }
        if(onTornado && press)
        {
            tornado.Jump();
        }
    }

    public void SetDash(bool press)
    {
        dashPressed = press;
    }

    public void SetMovementAxis(float h, float v)
    {
        if(onTornado)
        {
            tornado.SetMoveDir(h, v);
        }
        horizontal = h;
        vertical = v;
    }

    public void SetOnTornado(bool value)
    {
        onTornado = value;
        rigi.useGravity = !value;
        if(value)
        {
            tornado = transform.root.GetComponent<Tornado>();
        }
    }
}
