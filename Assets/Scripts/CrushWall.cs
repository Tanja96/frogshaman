﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrushWall : MonoBehaviour {

    public float backSpeed;
    public float forwardSpeed;
    public float movementDistance;
    public float frontDelay;
    public float backDelay;
    public float startDelay;

    private Vector3 backPos;
    private Vector3 frontPos;

    private void Start()
    {
        backPos = transform.position;
        frontPos = transform.position + transform.forward * movementDistance;
        StartCoroutine(StartCoro());
    }

    private IEnumerator StartCoro()
    {
        yield return new WaitForSeconds(startDelay);
        StartCoroutine(CrushWallMovement());
    }

    private IEnumerator CrushWallMovement()
    {
        yield return new WaitForSeconds(backDelay);
        float startTime = Time.time;
        while(transform.position != frontPos)
        {
            float distCovered = (Time.time - startTime) * forwardSpeed;
            float fracJourney = distCovered / movementDistance;
            transform.position = Vector3.Lerp(backPos, frontPos, fracJourney);
            yield return new WaitForFixedUpdate();
        }
        yield return new WaitForSeconds(frontDelay);
        startTime = Time.time;
        while(transform.position != backPos)
        {
            float distCovered = (Time.time - startTime) * backSpeed;
            float fracJourney = distCovered / movementDistance;
            transform.position = Vector3.Lerp(frontPos, backPos, fracJourney);
            yield return new WaitForFixedUpdate();
        }
        StartCoroutine(CrushWallMovement());
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<IDestroyable>() != null)
        {
            other.GetComponent<IDestroyable>().GetDamage(1000);
        }
    }
}
