﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiddleEnemy : Enemy {

    public EnemyElement element;
    public int spellDamage;
    public GameObject spell;
    protected bool spellUsed = false;
    protected float spellTimer = 3;
    protected float spellCastTime;

    protected override void Start()
    {
        base.Start();
        spellCastTime = Random.Range(1f, 3f);
    }

    protected override void Update()
    {
        base.Update();
        if (!flying && !agent.isStopped)
        {
            if (element != EnemyElement.none && state == AIState.attack)
            {
                spellTimer += Time.deltaTime;
                if (!spellUsed && spellTimer > spellCastTime)
                {
                    UseSpell();
                }
            }
        }
    }

    protected void UseSpell()
    {
        RaycastHit hit;
        if(!Physics.Raycast(transform.position, transform.forward, out hit, Vector3.Distance(frog.transform.position, transform.position)) || hit.transform == frog)
        {
            Vector3 pos = transform.position + transform.forward.normalized;
            GameObject obj = Instantiate(spell, pos, transform.rotation);
            obj.GetComponent<ASpell>().Cast();
            spellTimer = 0;
            spellCastTime = Random.Range(1f, 5f);
        }
    }

    public override void GetDamage(int amount)
    {
        health -= amount;
        if (health <= 0)
        {
            spawner.currentMiddleEnemies--;
            spawner.FreeSpot(homeSpot);
            Destroy(gameObject);
        }
    }
}
