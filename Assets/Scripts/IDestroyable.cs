﻿using UnityEngine;

public interface IDestroyable
{
    void GetDamage(int amount);
}
