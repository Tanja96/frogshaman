﻿using UnityEngine;

public class SmallEnemy : Enemy
{
    private float attackTimer;
    private bool attack = false;
    private Frog frogScript;
    public float attackTime;

    protected override void Update()
    {
        base.Update();
        if(attack)
        {
            attackTimer += Time.deltaTime;
            if(attackTimer >= attackTime)
            {
                frogScript.GetDamage(attackDamage);
                attackTimer = 0;
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            frogScript = collision.gameObject.GetComponent<Frog>();
            frogScript.GetDamage(attackDamage);
            attack = true;
            attackTimer = 0;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            attack = false;
            attackTimer = 0;
        }
    }

    public override void GetDamage(int amount)
    {
        health -= amount;
        if (health <= 0)
        {
            spawner.currentSmallEnemies--;
            spawner.FreeSpot(homeSpot);
            Destroy(gameObject);
        }
    }
}
