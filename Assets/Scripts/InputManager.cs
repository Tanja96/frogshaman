﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    FrogMovement movement;
    CameraMovement cameraMovement;
    Frog attack;
    UIManager ui;
    Aim aim;
    private bool uiActive;
    private bool aiming = false;

    private void Start()
    {
        movement = FindObjectOfType<FrogMovement>();
        attack = FindObjectOfType<Frog>();
        cameraMovement = FindObjectOfType<CameraMovement>();
        ui = FindObjectOfType<UIManager>();
        aim = FindObjectOfType<Aim>();
        //Cursor.visible = false;
    }

    private void Update()
    {
        if(uiActive)
        {
            ui.ChangeSelection(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            movement.SetMovementAxis(0,0);
        }
        else
        {
            if(aiming)
            {
                movement.SetMovementAxis(0,0);
            }
            else
            {
                movement.SetMovementAxis(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            }
        }
        if(aiming)
        {
            aim.SetMovementAxis(Input.GetAxis("Joystick X"), Input.GetAxis("Joystick Y"));
        }
        else
        {
            cameraMovement.SetMovementAxis(Input.GetAxis("Joystick X"), Input.GetAxis("Joystick Y"));
            if (Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0)
            {
                cameraMovement.SetMovementAxis(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
            }
        }

        if (Input.GetButtonDown("Jump"))
        {
            movement.SetJump(true);
        }
        else if (Input.GetButtonUp("Jump"))
        {
            movement.SetJump(false);
        }
        if (Input.GetButtonDown("Dash"))
        {
            movement.SetDash(true);
        }
        else if (Input.GetButtonUp("Dash"))
        {
            movement.SetDash(false);
        }
        if(Input.GetButtonDown("Attack"))
        {
            attack.SetAttack(true);
        }
        if (Input.GetButtonDown("CastSpell"))
        {
            attack.StartCast();
        }
        else if (Input.GetButtonUp("CastSpell"))
        {
            attack.StopCast();
        }
        if (Input.GetButtonDown("ChangeElement1"))
        {
            uiActive = true;
            ui.SetSelectPanel(true, 1);
        }
        else if (Input.GetButtonUp("ChangeElement1"))
        {
            uiActive = false;
            ui.SetSelectPanel(false, 1);
        }
        if (Input.GetButtonDown("ChangeElement2"))
        {
            uiActive = true;
            ui.SetSelectPanel(true, 2);
        }
        else if (Input.GetButtonUp("ChangeElement2"))
        {
            uiActive = false;
            ui.SetSelectPanel(false, 2);
        }
        //if (Input.GetButtonDown("Aim"))
        //{
        //    aiming = true;
        //    aim.StartAim();
        //}
        //else if (Input.GetButtonUp("Aim"))
        //{
        //    aiming = false;
        //    aim.EndAim();
        //}
        if (Input.GetButtonDown("Target"))
        {
            cameraMovement.SetTarget(true);
        }
        else if(Input.GetButtonUp("Target"))
        {
            cameraMovement.SetTarget(false);
        }
    }
}
