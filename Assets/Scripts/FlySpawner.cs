﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlySpawner : MonoBehaviour {
    public FlyElement element;
    public int flyAmount;
    public int maxFlies = 20;
    public float spawnRate;
    public Transform spawnPoint;
    public GameObject fly;

    private void Start()
    {
        StartCoroutine(spawnFly());    
    }

    void SpawnFlies()
    {
        GameObject spawned = Instantiate(fly);
        spawned.transform.position = spawnPoint.position + new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f));
        spawned.GetComponent<Flies>().element = element;
        spawned.GetComponent<Flies>().spawner = gameObject;
        spawned.transform.parent = transform;
        ++flyAmount;
    }

    IEnumerator spawnFly()
    {
        yield return new WaitForSeconds(spawnRate);
        if (flyAmount < maxFlies)
        {
            SpawnFlies();
        }
        StartCoroutine(spawnFly());
    }
}
