﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellNode {
    private GameObject spell;
    private SpellNode nWater;
    private SpellNode nIce;
    private SpellNode nAir;
    private SpellNode nSpark;
    private SpellNode nEarth;

    public SpellNode(GameObject sp, SpellNode nw, SpellNode ni, SpellNode na, SpellNode ns, SpellNode ne)
    {
        spell = sp;
        nWater = nw;
        nIce = ni;
        nAir = na;
        nSpark = ns;
        nEarth = ne;

    }

	public SpellNode NextSpell(FlyElement element)
    {
        if (element == FlyElement.water)
        {
            return nWater;
        }
        else if (element == FlyElement.ice)
        {
            return nIce;
        }
        else if (element == FlyElement.air)
        {
            return nAir;
        }
        else if (element == FlyElement.spark)
        {
            return nSpark;
        }
        else if (element == FlyElement.earth)
        {
            return nEarth;
        }
        return this;
    }
}
