﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManaPool : MonoBehaviour
{
    private int manaAmount;
    private float generationTimer;

    public float generationTime;
    public int maxManaAmount;
    public Image manaMeter;

    private void Start()
    {
        manaAmount = maxManaAmount;
    }

    private void Update()
    {
        if(manaAmount < maxManaAmount)
        {
            generationTimer += Time.deltaTime;
            if (generationTimer >= generationTime)
            {
                manaAmount++;
                UpdateUI();
                generationTimer = 0;
            }
        }
    }

    public int GetManaAmount()
    {
        return manaAmount;
    }

    public void DecreaseMana(int amount)
    {
        manaAmount -= amount;
        if(manaAmount < 0)
        {
            manaAmount = 0;
        }
        UpdateUI();
    }

    public void UpdateUI()
    {
        manaMeter.fillAmount = (float)manaAmount / (float)maxManaAmount;
    }
}
