﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodableObject : MonoBehaviour
{
    public void Explode(Vector3 position, float force, float radius)
    {
        foreach(Transform t in transform)
        {
            Rigidbody rigi = t.gameObject.AddComponent<Rigidbody>();
            rigi.AddExplosionForce(force, position, radius);
        }
        Destroy(gameObject, 3);
    }
}
