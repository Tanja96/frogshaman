﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;

public enum EnemyElement
{
    none,
    fire,
    water
}

public enum AIState
{
    relax,
    suspicious,
    attack,
    goHome
}

abstract public class Enemy : MonoBehaviour, IDestroyable, ISpellEffect {

    public int health;
    public Transform frog;
    public EnemySpawner spawner;
    public EnemyLocationSpot homeSpot;
    public int attackDamage;
    public AIState state;
    public int viewDistance;
    public int feelDistance;
    public float dotProductWantedValue;
    public float searchTime = 0;
    public SpriteRenderer sprite;
    public Sprite questionMark;
    public Sprite exlamationMark;
    public float wayPointStayTime;
    public float speed;
    public float slowDownSpeed;
    
    protected NavMeshAgent agent;
    protected bool rotatingInSearch;
    protected float searchTimer = 0;
    protected float startRotY;
    protected int waypointIndex = 0;
    protected Vector3 homePosition;
    protected bool flying = false;
    protected Rigidbody rigi;

    protected void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.speed = speed;
    }

    protected virtual void Start()
    {
        frog = FindObjectOfType<Frog>().transform;
        state = AIState.goHome;
        spawner.TakeSpot(homeSpot);
        if (homeSpot.type == EnemyLocationType.gather)
        {
            homePosition = new Vector3(homeSpot.transform.position.x + Random.Range(1, 3), homeSpot.transform.position.y, homeSpot.transform.position.z + Random.Range(1, 3));
            agent.SetDestination(homePosition);
        }
        else
        {
            agent.SetDestination(homeSpot.transform.position);
        }
    }

    protected virtual void Update()
    {
        if(!flying && !agent.isStopped)
        {
            switch(state)
            {
                case AIState.relax:
                    Relax();
                    break;
                case AIState.attack:
                    Attack();
                    break;
                case AIState.suspicious:
                    Suspicious();
                    break;
                case AIState.goHome:
                    GoHome();
                    break;
            }
        }
        else if(flying)
        {
            if(rigi.velocity.magnitude < 0.1)
            {
                if (Physics.Raycast(transform.position, -transform.up, 1.1f))
                {
                    agent.enabled = true;
                    rigi.isKinematic = true;
                    flying = false;
                }
            }
        }
    }

    protected void Relax()
    {
        if(homeSpot.type == EnemyLocationType.stand && transform.rotation != homeSpot.transform.rotation)
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, homeSpot.transform.rotation, 0.5f);
        }
        else if(homeSpot.type == EnemyLocationType.walk && agent.remainingDistance <= agent.stoppingDistance)
        {
            StartCoroutine(SetNextWaypoint());
        }
        float dist = Vector3.Distance(frog.position, transform.position);
        if ((dist < viewDistance && PlayerInView()) || dist < feelDistance)
        {
            transform.LookAt(frog);
            state = AIState.attack;
            sprite.sprite = exlamationMark;
        }
    }

    private IEnumerator SetNextWaypoint()
    {
        yield return new WaitForSeconds(wayPointStayTime);
        waypointIndex++;
        if(waypointIndex > homeSpot.wayPoints.Length-1)
        {
            waypointIndex = 0;
        }
        agent.SetDestination(homeSpot.wayPoints[waypointIndex].position);
    }

    protected void GoHome()
    {
        if(agent.remainingDistance <= agent.stoppingDistance)
        {
            state = AIState.relax;
        }
        float dist = Vector3.Distance(frog.position, transform.position);
        if ((dist < viewDistance && PlayerInView()) || dist < feelDistance)
        {
            transform.LookAt(frog);
            state = AIState.attack;
            sprite.sprite = exlamationMark;
        }
    }

    protected void Attack()
    {
        float dist = Vector3.Distance(frog.position, transform.position);
        if ((dist < viewDistance && PlayerInView()) || dist < feelDistance)
        {
            agent.SetDestination(frog.position);
        }
        else
        {
            state = AIState.suspicious;
            startRotY = transform.eulerAngles.y;
            agent.SetDestination(frog.position);
            sprite.sprite = questionMark;
        }
    }

    protected void Suspicious()
    {
        if(rotatingInSearch)
        {
            transform.rotation = Quaternion.Euler(0f, startRotY + 80 * Mathf.Sin(searchTimer * 2), 0f);
            float dist = Vector3.Distance(frog.position, transform.position);
            if ((dist < viewDistance && PlayerInView()) || dist < feelDistance)
            {
                state = AIState.attack;
                rotatingInSearch = false;
                searchTimer = 0;
                sprite.sprite = exlamationMark;
                transform.LookAt(frog);
            }
            searchTimer += Time.deltaTime;
            if(searchTimer > searchTime)
            {
                sprite.sprite = null;
                state = AIState.goHome;
                if (homeSpot.type == EnemyLocationType.gather)
                {
                    agent.SetDestination(homePosition);
                }
                else
                {
                    agent.SetDestination(homeSpot.transform.position);
                }
                rotatingInSearch = false;
                searchTimer = 0;
            }
        }
        else if(agent.remainingDistance <= agent.stoppingDistance)
        {
            rotatingInSearch = true;
            agent.ResetPath();
        }
    }
    
    protected bool PlayerInView()
    {
        Vector3 toOther = (frog.position - transform.position);
        if(Vector3.Dot(transform.forward, toOther.normalized) > dotProductWantedValue)
        {
            RaycastHit hit;
            if(Physics.Raycast(transform.position, toOther, out hit, toOther.magnitude))
            {
                if(hit.transform.CompareTag("Player"))
                {
                    return true;
                }
                else
                {
                    Debug.Log(hit.transform.name);
                }
            }
        }
        return false;
    }

    public virtual void GetDamage(int amount)
    {
        health -= amount;
        if (health <= 0)
        {
            spawner.FreeSpot(homeSpot);
            CameraMovement movement = FindObjectOfType<CameraMovement>();
            if (movement.enemies.Contains(transform))
            {
                movement.enemies.Remove(transform);
            }
            Destroy(gameObject);
        }
    }

    public void RiseSuspicion(Vector3 location)
    {
        state = AIState.suspicious;
        startRotY = transform.eulerAngles.y;
        if(agent.GetComponent<NavMeshAgent>().isActiveAndEnabled)
        {
            agent.SetDestination(location);
            sprite.sprite = questionMark;
        }
    }

    public void SetSpeed(bool normalSpeed)
    {
        if(normalSpeed)
        {
            agent.speed = speed;
        }
        else
        {
            agent.speed = slowDownSpeed;
        }
    }

    public void SetStopped(bool stopped)
    {
        if(agent.enabled)
        {
            agent.isStopped = stopped;
        }
    }

    public void SetStartLocation(Vector3 startLoc)
    {
        agent.Warp(startLoc);
    }

    public void MakeEnemyFly(Vector3 force)
    {
        agent.enabled = false;
        rigi = GetComponent<Rigidbody>();
        rigi.isKinematic = false;
        rigi.AddForce(force * 2);
        flying = true;
    }

    public void MakeEnemyFlyImpulse(Vector3 force)
    {
        agent.enabled = false;
        rigi = GetComponent<Rigidbody>();
        rigi.isKinematic = false;
        rigi.velocity = force;
        flying = true;
    }
}
