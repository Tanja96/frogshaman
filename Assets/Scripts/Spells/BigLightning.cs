﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigLightning : ASpell
{
    public ParticleSystem lightning;
    public ParticleSystem cloudSystem;
    public ParticleSystem cloudLightnings;
    public Collider triggerCollider;
    public float targetSize;
    public float startgrowthSpeed;
    public float endgrowthSpeed;
    public float cloudDuration;
    public float startDelay;
    public Light spotLight;
    public float lightningDelay;
    public float lightGrowSpeed;
    public int damage;
    public GameObject hurtingEffect;
    public float maxHurtingDistance;
    public LayerMask layerMask;
    public float distance;

    private bool casted = false;
    private bool lightningStruck = false;
    private ParticleSystem.VelocityOverLifetimeModule velocityModule;
    private ParticleSystem.EmissionModule emission;
    private float emissionAmount;
    private float timer = 0;
    private float angle = 1;
    private float lightningTimer = 0;
    private bool spotLightDone = false;

    private void Start()
    {
        var main = cloudSystem.main;
        main.duration = cloudDuration;
        main.startLifetime = cloudDuration;
        main = cloudLightnings.main;
        main.duration = cloudDuration;
        emission = cloudLightnings.emission;
        emissionAmount = emission.rateOverTimeMultiplier;
        velocityModule = cloudSystem.velocityOverLifetime;
        DeterminePosition();
    }

    private void Update()
    {
        if(casted && !spotLightDone)
        {
            if(spotLight.spotAngle < 60)
            {
                angle += Time.deltaTime * lightGrowSpeed;
                spotLight.spotAngle = angle;
            }
            else
            {
                lightningTimer += Time.deltaTime;
                if (lightningTimer > lightningDelay)
                {
                    StartLightning();
                }
            }
        }
        if(lightningStruck)
        {
            float multiplier = Mathf.Lerp(startgrowthSpeed, endgrowthSpeed, cloudLightnings.transform.localScale.x / targetSize);
            cloudLightnings.transform.localScale += new Vector3(1, 1, 1) * Time.deltaTime * multiplier;
            velocityModule.radial = multiplier * 1.4f;
            timer += Time.deltaTime;
            emission.rateOverTimeMultiplier = Mathf.Lerp(emissionAmount, 0, timer / cloudDuration);
        }
    }

    public override void Cast()
    {
        casted = true;
    }

    public void StartLightning()
    {
        spotLightDone = true;
        lightning.Play();
        StartCoroutine(StartDelay());
        spotLight.enabled = false;
    }

    private void DeterminePosition()
    {
        RaycastHit hit;
        Vector3 pos = transform.parent.position + (transform.parent.forward * distance + Vector3.up * 10);
        if(Physics.Raycast(pos, Vector3.down, out hit, 30, layerMask))
        {
            transform.parent = null;
            transform.position = hit.point;
        }
    }

    private IEnumerator StartDelay()
    {
        yield return new WaitForSeconds(startDelay);
        lightningStruck = true;
        cloudSystem.Play();
        cloudLightnings.Play();
        triggerCollider.enabled = true;
        Destroy(this.gameObject, cloudDuration + 0.1f);
    }

    private void OnTriggerEnter(Collider collider)
    {
        IDestroyable destroyable = collider.gameObject.GetComponent<IDestroyable>();
        if (destroyable != null)
        {
            float multiplier = 1 - (Vector3.Distance(collider.transform.position, transform.position) / maxHurtingDistance); 
            destroyable.GetDamage((int)(damage * multiplier));
            Instantiate(hurtingEffect, collider.transform);
            ISpellEffect spellEffect = collider.GetComponent<ISpellEffect>();
            if (spellEffect != null)
            {
                spellEffect.SetStopped(true);
            }
        }
    }
}
