﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMeltManager : BurnCounter
{
    protected override void BurnAction()
    {
        Destroy(this.gameObject);
    }
}
