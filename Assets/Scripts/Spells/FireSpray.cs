﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireSpray : ASpell
{
    public GameObject hitSystem;
    public GameObject flames;
    public int damage;

    private float timer = 0;
    private ParticleSystem particleEmitter;
    private List<ParticleCollisionEvent> collisionEvents;
    private ParticleSystem hitParticlesSystem;

    private void Start()
    {
        transform.position += transform.parent.forward * 0.5f;
        transform.Rotate(10, 0, 0);
        manaPool = FindObjectOfType<ManaPool>();
        if (manaPool != null && manaPool.GetManaAmount() <= manaUsage)
        {
            Destroy(this.gameObject);
        }
        particleEmitter = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();
        hitParticlesSystem = hitSystem.GetComponent<ParticleSystem>();
    }

    private void Update()
    {
        timer += Time.deltaTime;
        if (manaPool != null && timer >= 1)
        {
            manaPool.DecreaseMana(manaUsage);
            if (manaPool.GetManaAmount() < manaUsage)
            {
                Destroy(this.gameObject);
            }
            timer = 0;
        }
    }

    private void OnParticleCollision(GameObject other)
    {
        ParticlePhysicsExtensions.GetCollisionEvents(particleEmitter, other, collisionEvents);
        if (other.transform.root.GetComponent<BurnCounter>() != null)
        {
            other.transform.root.GetComponent<BurnCounter>().StartBurning(null);
        }
        hitSystem.transform.position = collisionEvents[0].intersection;
        hitParticlesSystem.Emit(1);

        if (other.layer == LayerMask.NameToLayer("Ground"))
        {
            other.GetComponent<IcePainter>().Erase(collisionEvents[0].intersection);
        }
    }

    public override void Cast()
    {
        Destroy(this.gameObject);
    }
}
