﻿using System;
using System.Collections;
using UnityEngine;

public class Speed : ASpell
{
    private int time;
    private float timer;
    private TextMesh textMesh;
    private bool started;
    private FlyManager manager;
    private Coroutine countDown;


    private void Start()
    {
        Speed[] speeds = FindObjectsOfType<Speed>();
        if (speeds.Length == 1)
        {
            textMesh = GetComponent<TextMesh>();
            transform.localPosition += new Vector3(0, 2, 0);
            manager = FindObjectOfType<FlyManager>();
        }
        else
        {
            for(int i = 0; i < speeds.Length; ++i)
            {
                if(speeds[i] != this)
                {
                    speeds[i].RestartTimer();
                    transform.parent.GetComponent<Frog>().currentSpell = speeds[i].gameObject;
                    break;
                }
            }
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        if (manager.elements[(int)FlyElement.spark] == 0)
        {
            Cast();
        }

        if (!started)
        {
            timer += Time.deltaTime;
            if(timer > 0.2f)
            {
                ++time;
                timer = 0;
                SetTime();
                manager.ChangeFlyCount(FlyElement.spark, -1);
            }
        }
    }

    public override void Cast()
    {
        if(!started)
        {
            started = true;
            countDown = StartCoroutine(CountDown());
            transform.parent.GetComponent<FrogMovement>().speedMultiplier = 2;
        }
    }

    public void RestartTimer()
    {
        started = false;
        StopCoroutine(countDown);
    }

    private IEnumerator CountDown()
    {
        WaitForSeconds delay = new WaitForSeconds(1);
        while(time > 0)
        {
            yield return delay;
            time--;
            SetTime();
        }
        transform.parent.GetComponent<FrogMovement>().speedMultiplier = 1;
        Destroy(gameObject);
    }

    private void SetTime()
    {
        TimeSpan ts = TimeSpan.FromSeconds(time);
        textMesh.text = string.Format(new DateTime(ts.Ticks).ToString("mm:ss"));
    }
}
