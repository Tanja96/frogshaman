﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceCheck : MonoBehaviour
{
    public bool icePossibilitie = false;
    public Texture2D tex;
    public LayerMask mask;
    public bool onIce = false;

    private RaycastHit hit;
    private bool once = false;

    private void Start()
    {
        tex = new Texture2D(1024, 1024, TextureFormat.ARGB32, false);
    }

    private void Update()
    {
        if(icePossibilitie && !once)
        {
            if(Physics.Raycast(transform.position, Vector3.down, out hit, 3, mask))
            {
                onIce = hit.transform.GetComponent<IcePainter>().IsOnIce(hit.textureCoord.x, hit.textureCoord.y);
            }
        }
    }

    public void SetInsideIceArea(bool value)
    {
        icePossibilitie = value;
        if (!value) onIce = false;
    }
}
