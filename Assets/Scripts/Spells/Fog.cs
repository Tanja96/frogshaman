﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fog : ASpell
{
    private void Start()
    {
        transform.position += Vector3.up * 0.5f;
        //FindObjectOfType<Frog>().isHidden = true;
    }
    

    public override void Cast()
    {
        FindObjectOfType<Frog>().isHidden = false;
        Destroy(gameObject);
    }
}
