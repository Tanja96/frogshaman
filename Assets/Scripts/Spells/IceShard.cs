﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceShard : MonoBehaviour
{
    public float freezeTime;
    private float curTime = 0;
    
    void Update()
    {
        curTime += Time.deltaTime;
        if(curTime >= freezeTime)
        {
            GetComponentInParent<FreezeCounter>().Melt();
            Destroy(gameObject);
        }
    }

    public void ResetTimer()
    {
        curTime = 0;
    }
}
