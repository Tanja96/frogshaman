﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rock : ASpell
{
    private Transform particles;
    private Rigidbody rigi;

    public float force;
    public int damage;
    public ParticleSystem rockParticles;
    public ParticleSystem hitParticles;

    private void Start()
    {
        manaPool = FindObjectOfType<ManaPool>();
        if (manaPool.GetManaAmount() <= manaUsage)
        {
            Destroy(this.gameObject);
        }
        rigi = GetComponent<Rigidbody>();
        transform.position += transform.parent.forward;
        particles = rockParticles.transform;
    }

    private void Update()
    {
        if(rigi.velocity.normalized != Vector3.zero)
        {
            particles.rotation = Quaternion.LookRotation(-rigi.velocity.normalized);
        }
    }

    public override void Cast()
    {
        manaPool.DecreaseMana(manaUsage);
        rockParticles.Play();
        rigi.useGravity = true;
        rigi.isKinematic = false;
        GetComponent<Collider>().enabled = true;
        transform.parent = null;
        rigi.AddForce((transform.forward + transform.up * 0.1f) * force, ForceMode.Impulse);
        Destroy(this.gameObject, 3);
    }

    private void OnCollisionEnter(Collision collision)
    {
        IDestroyable destroyable = collision.gameObject.GetComponent<IDestroyable>();
        if (destroyable != null)
        {
            destroyable.GetDamage(damage);
        }
        hitParticles.Play();
        hitParticles.transform.parent = null;
        hitParticles.transform.position = collision.contacts[0].point;
        hitParticles.transform.rotation = Quaternion.LookRotation(collision.contacts[0].normal);
        Destroy(this.gameObject);
    }
}
