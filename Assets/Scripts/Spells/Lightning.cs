﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lightning : ASpell
{
    public LineRenderer line;
    public float length;
    public LayerMask layerMask;
    public float lifeTime;
    public Transform movingSystem;
    public ParticleSystem[] particleSystems;
    public ParticleSystem startLightnings;
    public ParticleSystem hitSystem;
    public ParticleSystem failSystem;
    public float loadingTime;
    public int damage;
    public GameObject hurtingEffect;
    [Range(0,1)]
    public float hurtingPossibilitie;

    private bool casted = false;
    private Transform movingTarget = null;
    private Vector3 targetPos;
    private float timer = 0;
    private float loadingTimer = 0;
    private bool loaded = false;
    private bool hitSystemPlaying = false;
    private bool hitSomething = false;

    private void Start()
    {
        transform.position += transform.parent.forward * 1f;
        manaPool = FindObjectOfType<ManaPool>();
        if (manaPool != null && manaPool.GetManaAmount() <= manaUsage)
        {
            Destroy(this.gameObject);
        }
    }

    private void Update()
    {
        if (casted && loaded)
        {
            if(movingTarget != null)
            {
                targetPos = transform.InverseTransformPoint(movingTarget.position);
                line.SetPosition(1, targetPos);
            }
            timer += Time.deltaTime / lifeTime;
            movingSystem.localPosition = Vector3.Lerp(Vector3.zero, targetPos, timer);
            if(hitSomething && lifeTime - timer < 0.1f && !hitSystemPlaying)
            {
                hitSystem.transform.localPosition = targetPos - targetPos.normalized * 0.5f;
                hitSystem.transform.parent = null;
                hitSystemPlaying = true;
                hitSystem.Play();
                if(movingTarget)
                {
                    float rand = Random.Range(0f, 1f);
                    if(rand > 1 - hurtingPossibilitie)
                    {
                        Instantiate(hurtingEffect, movingTarget);
                        if(movingTarget.GetComponent<ISpellEffect>() != null)
                        {
                            movingTarget.GetComponent<ISpellEffect>().SetStopped(true);
                        }
                    }
                    IDestroyable destroyable = movingTarget.GetComponent<IDestroyable>();
                    if (destroyable != null)
                    {
                        destroyable.GetDamage(damage);
                    }
                }
            }
        }
        else if(!loaded && !casted)
        {
            loadingTimer += Time.deltaTime;
            if (loadingTimer >= loadingTime)
            {
                startLightnings.Play();
                loaded = true;
            }
        }
    }

    public override void Cast()
    {
        if(loaded)
        {
            line.enabled = true;
            movingSystem.GetComponent<ParticleSystem>().Play();
        }
        else
        {
            Instantiate(failSystem, transform.position + transform.forward * 0.7f, Quaternion.identity);
            Debug.Log(transform.position + transform.forward * 0.75f);
        }
        //manaPool.DecreaseMana(manaUsage);
        casted = true;
        if(CheckForTargets() == false)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit, length))
            {
                hitSomething = true;
                targetPos = transform.InverseTransformPoint(hit.point);
            }
            else
            {
                targetPos = transform.InverseTransformPoint(transform.position + transform.forward * length);
            }
        }
        line.SetPosition(1, targetPos);
        if(movingTarget != null && movingTarget.GetComponent<ILightningEffect>() != null && loaded)
        {
            movingTarget.GetComponent<ILightningEffect>().Electrocute();
        }
        StartCoroutine(Stopper());
        startLightnings.Stop();
    }

    private IEnumerator Stopper()
    {
        yield return new WaitForSeconds(lifeTime);
        line.enabled = false;
        foreach (ParticleSystem s in particleSystems)
        {
            s.Stop();
        }
        Destroy(gameObject, 1);
    }

    private bool CheckForTargets()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, length, layerMask);
        Transform target = null;
        float bestValue = -1;
        foreach(Collider c in colliders)
        {
            float dot = Vector3.Dot(transform.forward, c.transform.position - transform.position);
            if (dot > bestValue)
            {
                bestValue = dot;
                target = c.transform;
            }
        }
        if(bestValue > 0.5f)
        {
            hitSomething = true;
            targetPos = transform.InverseTransformPoint(target.position);
            movingTarget = target;
            return true;
        }
        return false;
    }
}
