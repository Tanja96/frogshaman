﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurnableIceCounter : FreezeCounter
{
    protected override void FreezeAction()
    {
        GetComponent<BurnCounter>().StopBurning();
    }

    protected override void SlowDownAction()
    {
        
    }
}
