﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IcePainter : MonoBehaviour
{
    //For drawing
    private RenderTexture drawTexture;
    private Material groundMaterial;
    private Material drawMaterial;

    //For raycast
    private RaycastHit hit;
    private LayerMask mask;

    //For collider growth
    private BoxCollider triggerCollider;
    private Vector3 maxBounds = Vector3.negativeInfinity;
    private Vector3 minBounds = Vector3.positiveInfinity;

    //For IceCheck
    private readonly bool[,] iceList = new bool[1024, 1024];
    private float drawWidth;

    private void Start()
    {
        mask = LayerMask.GetMask("Ground");

        Vector3 bounds = GetComponent<MeshRenderer>().bounds.size;
        drawWidth = bounds.magnitude * 1.2f;

        drawMaterial = new Material(Shader.Find("Custom/DrawShader"));
        drawMaterial.SetFloat("_DrawWidth", drawWidth);
        drawMaterial.SetFloat("_Strength", 1);
        drawMaterial.SetVector("_Color", Color.red);

        groundMaterial = GetComponent<MeshRenderer>().material;
        drawTexture = new RenderTexture(1024, 1024, 0, RenderTextureFormat.ARGB32);
        groundMaterial.SetTexture("_MapTex", drawTexture);
        groundMaterial.SetTextureScale("_IceTex", new Vector2(1, 1) * bounds.magnitude * 0.1f);
    }

    public void Paint(Vector3 pos)
    {
        if (Physics.Raycast(pos + Vector3.up * 0.1f, Vector3.down, out hit, 1, mask))
        {
            if (hit.transform == transform)
            {
                if (triggerCollider == null)
                {
                    triggerCollider = gameObject.AddComponent<BoxCollider>();
                    triggerCollider.isTrigger = true;
                }
                drawMaterial.SetVector("_Coordinate", new Vector4(hit.textureCoord.x, hit.textureCoord.y, 0, 0));
                drawMaterial.SetFloat("_Dir", 1);

                RenderTexture temp = RenderTexture.GetTemporary(drawTexture.width, drawTexture.height, 0, RenderTextureFormat.ARGBFloat);
                Graphics.Blit(drawTexture, temp);
                Graphics.Blit(temp, drawTexture, drawMaterial);
                RenderTexture.ReleaseTemporary(temp);
                
                FillList((int)(hit.textureCoord.x * 1024), (int)(hit.textureCoord.y * 1024), true);
                SetCollider(hit.point);
            }
        }
    }

    public void Erase(Vector3 pos)
    {
        if (Physics.Raycast(pos + Vector3.up * 0.1f, Vector3.down, out hit, 1, mask))
        {
            if (hit.transform == transform)
            {
                drawMaterial.SetVector("_Coordinate", new Vector4(hit.textureCoord.x, hit.textureCoord.y, 0, 0));
                drawMaterial.SetFloat("_Dir", -1);

                RenderTexture temp = RenderTexture.GetTemporary(drawTexture.width, drawTexture.height, 0, RenderTextureFormat.ARGBFloat);
                Graphics.Blit(drawTexture, temp);
                Graphics.Blit(temp, drawTexture, drawMaterial);
                RenderTexture.ReleaseTemporary(temp);

                FillList((int)(hit.textureCoord.x * 1024), (int)(hit.textureCoord.y * 1024), false);
            }
        }
    }

    public void FillList(int x, int y, bool value)
    {
        int width = (int)drawWidth / 12;
        int minX = (int)(Mathf.InverseLerp(0, 1023, x - width) * 1023);
        int maxX = (int)(Mathf.InverseLerp(0, 1023, x + width) * 1023);
        int minY = (int)(Mathf.InverseLerp(0, 1023, y - width) * 1023);
        int maxY = (int)(Mathf.InverseLerp(0, 1023, y + width) * 1023);
        for (int i = minX; i < maxX; i++)
        {
            for(int j = minY; j < maxY; j++)
            {
                iceList[i, j] = value;
            }
        }
    }

    private void SetCollider(Vector3 position)
    {
        Vector3 pos = transform.InverseTransformPoint(position);
        if (pos.x < minBounds.x)
        {
            minBounds.x = pos.x;
        }
        else if (pos.x > maxBounds.x)
        {
            maxBounds.x = pos.x;
        }
        if (pos.z < minBounds.z)
        {
            minBounds.z = pos.z;
        }
        else if (pos.z > maxBounds.z)
        {
            maxBounds.z = pos.z;
        }
        if (maxBounds.x - minBounds.x > 0 && maxBounds.z - minBounds.z > 0)
        {
            Vector3 size = maxBounds - minBounds;
            size.y = 1;
            Vector3 center = (maxBounds + minBounds) / 2;
            center.y = 0;
            triggerCollider.size = size;
            triggerCollider.center = center;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        IceCheck ice = other.GetComponent<IceCheck>();
        if (ice != null)
        {
            ice.SetInsideIceArea(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        IceCheck ice = other.GetComponent<IceCheck>();
        if (ice != null)
        {
            ice.SetInsideIceArea(false);
        }
    }

    public bool IsOnIce(float xCoord, float yCoord)
    {
        int x = (int)(xCoord * 1023);
        int y = (int)(yCoord * 1023);
        return iceList[x, y];
    }
}
