﻿using System.Collections;
using UnityEngine;

public class SnowStorm : ASpell
{
    public GameObject startEffect;
    public ParticleSystem cloud;
    public ParticleSystem[] snow;
    public ParticleSystem explosion;
    public Collider ownCollider;
    public float effectHeight;
    public float duration;
    public float cloudSize;
    public float speed;

    private bool spellCasted = false;
    private bool cloudFormed = false;
    private bool snowStarted = false;
    private float startHeight;
    private Vector3 forward;

    private void Start()
    {
        transform.position += transform.parent.forward * 1f;
        manaPool = FindObjectOfType<ManaPool>();
        if (manaPool != null && manaPool.GetManaAmount() <= manaUsage)
        {
            Destroy(this.gameObject);
        }
    }

    private void Update()
    {
        if(spellCasted && !cloudFormed)
        {
            if(transform.position.y < startHeight + effectHeight)
            {
                transform.position += (forward + new Vector3(0, 0.5f, 0)) * Time.deltaTime * speed;
            }
            else if(!explosion.isPlaying)
            {
                Destroy(startEffect);
                explosion.Play();
                var main = cloud.main;
                main.startLifetime = duration;
                main.duration = duration;
                cloudFormed = true;
                cloud.Play();
            }
        }
        else if(cloudFormed && !snowStarted)
        {
            if(cloud.transform.localScale.x < cloudSize)
            {
                cloud.transform.localScale += new Vector3(1,1,1) * Time.deltaTime * 1.3f;
            }
            else
            {
                foreach(ParticleSystem effect in snow)
                {
                    var main = effect.main;
                    main.duration = duration - 1;
                    effect.Play();
                }
                Destroy(gameObject, duration);
                snowStarted = true;
            }
        }
    }

    public override void Cast()
    {
        spellCasted = true;
        forward = transform.parent.forward;
        transform.parent = null;
        startHeight = transform.position.y;
    }

    private void OnTriggerStay(Collider other)
    {
        if(snowStarted)
        {
            FreezeCounter counter;
            if(counter = other.GetComponent<FreezeCounter>())
            {
                counter.Freeze();
            }
        }
    }
}
