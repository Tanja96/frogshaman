﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISpellEffect
{
    void SetSpeed(bool normalSpeed);
    void SetStopped(bool stopped);
}
