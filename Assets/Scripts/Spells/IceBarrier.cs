﻿using UnityEngine;
using UnityEngine.AI;

public class IceBarrier : ASpell
{
    public float movementSpeed;
    public float movementDistance;
    public float meltingTime;
    public int damage;
    public float explosionForce;
    public ParticleSystem[] systems;
    public NavMeshObstacle obstacle;
    public BoxCollider boxCollider;
    public BoxCollider triggerCollider;

    private bool casted = false;
    private Vector3 startPos;
    private bool stopped = false;

    private void Start()
    {
        manaPool = FindObjectOfType<ManaPool>();
        if(manaPool.GetManaAmount() < manaUsage)
        {
            Destroy(this.gameObject);
        }
        transform.position += transform.forward;
        startPos = transform.position;
        ParticleSystem.MainModule main;
        foreach (ParticleSystem system in systems)
        {
            main = system.main;
            main.startLifetime = meltingTime;
        }
    }

    private void Update()
    {
        if(casted && !stopped)
        {
            if(Vector3.Distance(startPos, transform.position) < movementDistance)
            {
                float amount = Time.deltaTime * movementSpeed;
                transform.position += -transform.up * amount;
                boxCollider.size += new Vector3(0, amount, 0);
                boxCollider.center += new Vector3(0, amount * 0.5f, 0);
                triggerCollider.size += new Vector3(0, amount, 0);
                triggerCollider.center += new Vector3(0, amount * 0.5f, 0);
                obstacle.size += new Vector3(0, amount, 0);
                obstacle.center += new Vector3(0, amount * 0.5f, 0);
            }
            else
            {
                foreach(ParticleSystem system in systems)
                {
                    system.Stop();
                }
                stopped = true;
            }
        }
        else if(stopped)
        {
            if(systems[0].particleCount == 0)
            {
                Destroy(this.gameObject);
            }
        }
    }

    public override void Cast()
    {
        transform.parent = null;
        Vector3 eulers = transform.eulerAngles;
        eulers.x = -90;
        transform.eulerAngles = eulers;

        RaycastHit hit;
        if(Physics.Raycast(transform.position, Vector3.down, out hit, 10))
        {
            transform.position = new Vector3(transform.position.x, hit.point.y, transform.position.z);
        }

        casted = true;
        foreach (ParticleSystem system in systems)
        {
            system.Play();
        }

        manaPool.DecreaseMana(manaUsage);
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Doesn't hit the player if it is still being casted
        //if ((!stopped && collision.transform.tag != "Player" && collision.transform.GetComponent<IDestroyable>() != null) || (stopped && collision.transform.GetComponent<IDestroyable>() != null))
        if (stopped && collision.transform.tag == "Player")
        {
            collision.rigidbody.AddForce(-collision.GetContact(0).normal * explosionForce, ForceMode.Impulse);
            collision.transform.GetComponent<IDestroyable>().GetDamage(damage);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Enemy"))
        {
            other.GetComponent<IDestroyable>().GetDamage(damage);
        }
    }
}
