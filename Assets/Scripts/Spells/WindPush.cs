﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindPush : ASpell
{
    private List<ParticleCollisionEvent> collisionEvents;

    public ParticleSystem windSystem1;
    public ParticleSystem windSystem2;
    public ParticleSystem vortexSystem;
    public ParticleSystem startSystem;
    public float force;

    private void Start()
    {
        collisionEvents = new List<ParticleCollisionEvent>();
        transform.position += transform.parent.forward * 1;
        manaPool = FindObjectOfType<ManaPool>();
        if (manaPool != null && manaPool.GetManaAmount() <= manaUsage)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnParticleCollision(GameObject other)
    {
        if (other.layer == LayerMask.NameToLayer("SpellEffects"))
        {
            ParticlePhysicsExtensions.GetCollisionEvents(windSystem1, other, collisionEvents);
            if (collisionEvents.Count == 0)
            {
                ParticlePhysicsExtensions.GetCollisionEvents(windSystem2, other, collisionEvents);
            }
            other.GetComponent<Tornado>().GetBigger(collisionEvents[0].velocity);
        }
        else if(other.layer == LayerMask.NameToLayer("Enemy"))
        {
            ParticlePhysicsExtensions.GetCollisionEvents(windSystem1, other, collisionEvents);
            if (collisionEvents.Count == 0)
            {
                ParticlePhysicsExtensions.GetCollisionEvents(windSystem2, other, collisionEvents);
            }
            other.GetComponent<Enemy>().MakeEnemyFly(collisionEvents[0].velocity * force);
        }
        else
        {
            Rigidbody rigi = other.GetComponent<Rigidbody>();
            if(rigi != null)
            {
                ParticlePhysicsExtensions.GetCollisionEvents(windSystem1, other, collisionEvents);
                if(collisionEvents.Count == 0)
                {
                    ParticlePhysicsExtensions.GetCollisionEvents(windSystem2, other, collisionEvents);
                }
                other.GetComponent<Rigidbody>().AddForce(collisionEvents[0].velocity * force, ForceMode.Force);
            }
        }
    }

    public override void Cast()
    {
        windSystem1.Play();
        windSystem2.Play();
        vortexSystem.Play();
        startSystem.Stop();
        manaPool.DecreaseMana(manaUsage);
    }
}
