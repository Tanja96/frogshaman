﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : ASpell {

    public int damage;
    public int burnAmount;
    public SphereCollider ownCollider;
    public LayerMask layers;
    public float speed;
    public float rotationSpeed;
    public ParticleSystem tail;
    public ParticleSystem ball;
    public GameObject fireHit;

    private bool spellCasted = false;
    private Transform target;
    private bool playerSpell;
    private Rigidbody rigi;
    private Vector3 lookAtDir;

    private void Start()
    {
        if(transform.parent != null)
        {
            transform.position += transform.forward.normalized;
            manaPool = FindObjectOfType<ManaPool>();
            if (manaPool.GetManaAmount() <= manaUsage)
            {
                Destroy(this.gameObject);
            }
            playerSpell = true;
            var force = ball.forceOverLifetime;
            force.yMultiplier = 0.3f;
        }
        else
        {
            playerSpell = false;
            spellCasted = true;
        }
    }

    private void FixedUpdate()
    {
        if(spellCasted)
        {
            if(target != null)
            {
                lookAtDir = (target.position - transform.position).normalized;
                Vector3 dir = Vector3.RotateTowards(transform.forward, lookAtDir, rotationSpeed * Time.deltaTime, 0.0f);
                transform.rotation = Quaternion.LookRotation(dir);
            }
            else if(playerSpell)
            {
                Collider[] objects = Physics.OverlapSphere(transform.position, 30f, layers);
                Transform closest = null;
                float dist = 1000;
                foreach(Collider o in objects)
                {
                    if(Vector3.Dot(transform.forward, (o.transform.position - transform.position)) > 0.5f)
                    {
                        float d = Vector3.Distance(transform.position, o.transform.position);
                        if (d < dist)
                        {
                            closest = o.transform;
                            dist = d;
                        }
                    }
                }
                if(closest != null)
                {
                    target = closest;
                }
            }
            else
            {
                target = FindObjectOfType<Frog>().transform;
            }
            rigi.MovePosition(transform.position + transform.forward * speed);
        }
    }

    public override void Cast()
    {
        rigi = GetComponent<Rigidbody>();
        if (playerSpell)
        {
            target = FindObjectOfType<CameraMovement>().GetTarget();
            manaPool.DecreaseMana(manaUsage);
        }
        rigi.isKinematic = false;
        transform.parent = null;
        ownCollider.enabled = true;
        Destroy(gameObject, 3);
        spellCasted = true;
        tail.Play();

        var force = ball.forceOverLifetime;
        force.yMultiplier = 0;
        force.zMultiplier = -0.55f;
        var main = ball.main;
        main.simulationSpeed = 4;
    }

    protected void OnCollisionEnter(Collision collision)
    {
        if(spellCasted)
        {
            if (collision.transform.root.GetComponent<BurnCounter>() != null)
            {
                if (collision.gameObject.GetComponent<IDestroyable>() != null)
                {
                    collision.gameObject.GetComponent<IDestroyable>().GetDamage(damage);
                }
                collision.transform.root.GetComponent<BurnCounter>().StartBurning(burnAmount);
            }
            Collider[] colliders = Physics.OverlapSphere(transform.position, 10);
            foreach(Collider collider in colliders)
            {
                Enemy enemy = collider.transform.GetComponent<Enemy>();
                if (enemy != null)
                {
                    enemy.RiseSuspicion(transform.position);
                }
            }
            Instantiate(fireHit, collision.contacts[0].point, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
