﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : ASpell
{
    private float explosionForce = 10.0f;
    private float timer = 0;
    private FlyManager manager;

    private void Start()
    {
        manager = FindObjectOfType<FlyManager>();
        if (manager.elements[(int)FlyElement.earth] <= 0)
        {
            Destroy(gameObject);
        }
        else
        {
            manager.ChangeFlyCount(FlyElement.earth, -1);
        }
    }

    private void Update()
    {
        timer += Time.deltaTime;
        if (timer > 1)
        {
            manager.ChangeFlyCount(FlyElement.earth, -1);
            timer = 0;
        }
        if (manager.elements[(int)FlyElement.earth] <= 0)
        {
            Cast();
        }
    }

    public override void Cast()
    {
        Rigidbody own = GetComponent<Rigidbody>();
        Rigidbody[] children = transform.GetComponentsInChildren<Rigidbody>();
        foreach(Rigidbody child in children)
        {
            if(child != own)
            {
                child.isKinematic = false;
                child.AddExplosionForce(explosionForce, transform.position, 5, 0, ForceMode.Impulse);
                child.gameObject.AddComponent<Dart>();
                child.transform.parent = null;
                Destroy(child.gameObject, 1);
            }
        }
        Destroy(gameObject);
    }
}
