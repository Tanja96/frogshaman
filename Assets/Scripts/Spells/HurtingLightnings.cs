﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtingLightnings : MonoBehaviour
{
    public float duration;
    public int damage;
    public float damageTime;
    public LayerMask mask;
    
    private float timer;

    void Start()
    {
        if(transform.parent != null)
        {
            transform.localPosition = new Vector3(0, 0, 0);
        }
        ParticleSystem system = GetComponent<ParticleSystem>();
        var main = system.main;
        main.duration = duration;
        system.Play();
    }
    
    void Update()
    {
        timer += Time.deltaTime;
        if(timer > damageTime)
        {
            Collider[] colliders = Physics.OverlapSphere(transform.position, 1, mask);
            foreach(Collider c in colliders)
            {
                c.GetComponent<IDestroyable>().GetDamage(damage);
            }
            timer = 0;
        }
    }

    private void OnDestroy()
    {
        if (transform.parent != null && transform.parent.GetComponent<ISpellEffect>() != null)
        {
            transform.parent.GetComponent<ISpellEffect>().SetStopped(false);
        }
    }
}
