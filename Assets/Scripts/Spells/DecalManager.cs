﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecalManager : MonoBehaviour
{
    private ParticleSystem decalSystem;
    private Vector3 lastEmitPos;
    private ParticleSystem.EmitParams emitParams;

    public float decalLifetime;
    public float minMovementDistance = 0.3f;

    private void Start()
    {
        decalSystem = GetComponent<ParticleSystem>();
        lastEmitPos = transform.position;
    }

    public void AddDecal(Vector3 location, Vector3 normal)
    {
        transform.position = location;
        if (Vector3.Distance(lastEmitPos, transform.position) > minMovementDistance)
        {
            emitParams = new ParticleSystem.EmitParams
            {
                position = location,
                rotation3D = Quaternion.LookRotation(normal).eulerAngles,
                startLifetime = decalLifetime
            };
            decalSystem.Emit(emitParams, 1);
            lastEmitPos = transform.position;
        }
    }
}
