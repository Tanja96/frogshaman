﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaBomb : ASpell
{
    private Rigidbody rigi;
    private MaterialPropertyBlock props;
    private MeshRenderer meshRenderer;
    private float speed;
    private Light bombLight;
    public bool objectHit = false;

    public float force;
    public int damage;
    public float speedGain;
    public float speedMultiplier;
    public float startSpeed;
    public GameObject bombExplosion;
    public float explosionRadius;
    public float explosionForce;

    private void Start()
    {
        //manaPool = FindObjectOfType<ManaPool>();
        //if (manaPool.GetManaAmount() <= manaUsage)
        //{
        //    Destroy(this.gameObject);
        //}
        //transform.position += transform.parent.forward;
        rigi = GetComponent<Rigidbody>();
        props = new MaterialPropertyBlock();
        meshRenderer = GetComponent<MeshRenderer>();
        speed = startSpeed;
        bombLight = GetComponent<Light>();
    }

    private void Update()
    {
        if(objectHit)
        {
            speed += Time.deltaTime * speedMultiplier;
            props.SetFloat("_Speed", speed * speed);
            meshRenderer.SetPropertyBlock(props);
            bombLight.intensity = Mathf.Sin(speed * speed) * 0.75f;
            if(speed > speedGain + startSpeed)
            {
                Instantiate(bombExplosion, transform.position, Quaternion.Euler(-90, 0, 0));
                Explode();
                Destroy(gameObject);
            }
        }
    }

    private void Explode()
    {
        Collider[] hits = Physics.OverlapSphere(transform.position, explosionRadius);
        foreach(Collider c in hits)
        {
            Rigidbody r;
            if(c.gameObject.layer == LayerMask.NameToLayer("Enemy"))
            {
                Vector3 dir = c.transform.position - transform.position;
                float dist = 1 - (dir.magnitude / explosionRadius) + 0.1f; // if enemy is close the force is bigger
                c.GetComponent<IDestroyable>().GetDamage((int)(damage * dist));
                c.GetComponent<Enemy>().MakeEnemyFlyImpulse(explosionForce * 0.1f * dir.normalized * dist);
            }
            else if(c.GetComponent<ExplodableObject>())
            {
                c.GetComponent<ExplodableObject>().Explode(transform.position, explosionForce, explosionRadius);
            }
            else if(r = c.GetComponent<Rigidbody>())
            {
                r.AddExplosionForce(explosionForce, transform.position, explosionRadius);
            }
        }
    }

    public override void Cast()
    {
        //manaPool.DecreaseMana(manaUsage);
        rigi.useGravity = true;
        rigi.isKinematic = false;
        GetComponent<Collider>().enabled = true;
        transform.parent = null;
        rigi.AddForce((transform.forward + transform.up * 0.1f) * force, ForceMode.Impulse);
    }

    private void OnCollisionEnter(Collision collision)
    {
        objectHit = true;
    }
}
