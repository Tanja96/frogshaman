﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tornado : ASpell
{
    public ParticleSystem startSystem;
    public ParticleSystem[] effectSystems;
    public BoxCollider trigger;
    public BoxCollider ownCollider;
    public Transform frogPos;
    public float bigTornadoLifeTime;
    public float lifeTime;
    public float movementSpeed;
    public int manaUsageWhenPlayerIsOn;
    public float manaUseTime;
    public float force;
    
    private Transform player;
    private Rigidbody playerRigi;
    private Rigidbody rigi;
    private Vector3 moveDir = Vector3.zero;
    private bool getBiggerStarted = false;
    private float timer;
    private bool stopped = false;
    private Transform cameraObject;
    private float manaTimer;
    private bool onTop = false;
    private float sinTimer;
    private bool jumpPressed;

    private void Start()
    {
        transform.position += transform.parent.forward * 1f;
        manaPool = FindObjectOfType<ManaPool>();
        if (manaPool != null && manaPool.GetManaAmount() <= manaUsage)
        {
            Destroy(this.gameObject);
        }
        cameraObject = FindObjectOfType<CameraMovement>().transform;
    }

    private void Update()
    {
        if(!stopped)
        {
            timer += Time.deltaTime;
            if(timer > lifeTime)
            {
                StopEffects();
                stopped = true;
            }
        }
    }

    private void FixedUpdate()
    {
        if (player)
        {
            if(onTop) // moves the tornado with player input and moves player up and down
            {
                transform.position += moveDir;
                if(!jumpPressed)
                {
                    sinTimer += Time.deltaTime;
                    player.position = frogPos.position + new Vector3(0, Mathf.Sin(sinTimer * 4) * 0.15f, 0);
                }
            }
            else // moves player to the top of the tornado.
            {
                player.position = Vector3.MoveTowards(player.position, frogPos.position, Time.deltaTime * 4);
                if (Vector3.Distance(player.position, frogPos.position) < 0.02f)
                {
                    onTop = true;
                    sinTimer = 0.15f;
                    playerRigi.velocity = Vector3.zero;
                }
            }
            manaTimer += Time.deltaTime;
            if (manaTimer > manaUseTime)
            {
                manaPool.DecreaseMana(manaUsageWhenPlayerIsOn);
                manaTimer = 0;
                timer -= manaUseTime;
                if (manaPool.GetManaAmount() <= manaUsage)
                {
                    StopEffects();
                    stopped = true;
                }
            }
        }
        else if (moveDir != Vector3.zero)
        {
            rigi.MovePosition(transform.position + moveDir);
        }
    }

    public override void Cast()
    {
        manaPool.DecreaseMana(manaUsage);
        rigi = gameObject.AddComponent<Rigidbody>();
        rigi.constraints = RigidbodyConstraints.FreezeRotation;
        rigi.AddForce(transform.forward + transform.up, ForceMode.Impulse);
        transform.parent = null;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            startSystem.Stop();
            foreach (ParticleSystem effect in effectSystems)
            {
                effect.Play();
            }
            trigger.enabled = true;
        }
        if(collision.gameObject.layer == LayerMask.NameToLayer("Enemy"))
        {
            Vector3 dir = collision.transform.position - transform.position;
            dir.y = 0;
            dir = dir.normalized;
            dir.y = 0.5f;
            collision.gameObject.GetComponent<Enemy>().MakeEnemyFlyImpulse(dir * force);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(!stopped && other.tag == "Player")
        {
            other.transform.parent = transform;
            other.GetComponent<FrogMovement>().SetOnTornado(true);
            playerRigi = other.GetComponent<Rigidbody>();
            player = other.transform;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(!stopped && other.tag == "Player")
        {
            other.transform.parent = null;
            other.GetComponent<FrogMovement>().SetOnTornado(false);
            player = null;
            onTop = false;
            jumpPressed = false;
        }
    }

    public void GetBigger(Vector3 dir)
    {
        if(!stopped && getBiggerStarted == false)
        {
            int index = 0;
            foreach(var effect in effectSystems)
            {
                var main = effect.main;
                main.startLifetime = bigTornadoLifeTime;
                if(index == 0)
                {
                    main.startSize = 7;
                }
                else if(index == 1)
                {
                    main.startSize = 3.5f;
                }
                index++;
            }
            lifeTime = bigTornadoLifeTime;
            timer = 0;
            moveDir = dir * 0.015f;
            getBiggerStarted = true;
            ownCollider.center = new Vector3(0, ownCollider.size.y, 0);
            ownCollider.size *= 2;
        }
    }

    public void SetMoveDir(float h, float v)
    {
        Vector3 forward = new Vector3(cameraObject.forward.x, 0, cameraObject.forward.z).normalized;
        Vector3 right = new Vector3(cameraObject.right.x, 0, cameraObject.right.z).normalized;
        Vector3 direction = right * h + forward * v;
        moveDir = direction * movementSpeed;
    }
    
    public void Jump()
    {
        jumpPressed = true;
    }

    private void StopEffects()
    {
        foreach (var effect in effectSystems)
        {
            effect.Stop();
        }
        Destroy(this.gameObject, bigTornadoLifeTime);
        if(player != null)
        {
            player.GetComponent<FrogMovement>().SetOnTornado(false);
            player.parent = null;
            player = null;
        }
    }
}
