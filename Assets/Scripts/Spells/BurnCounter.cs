﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurnCounter : MonoBehaviour
{
    private int burnCount;
    private float lastBurn;
    private BurnableObject burnableObject = null;
    private GameObject obj;
    private FreezeCounter freezeCounter;
    private IDestroyable destroyable = null;
    
    public bool burn;

    public GameObject flames;
    public int hitsBeforeBurn;

    private void Start()
    {
        burnableObject = GetComponent<BurnableObject>();
        freezeCounter = GetComponent<FreezeCounter>();
        destroyable = GetComponent<IDestroyable>();
    }

    public void StartBurning(int? count)
    {
        if (!burn)
        {
            if (Time.time - lastBurn >= 1)
            {
                burnCount = 0;
            }
            if(count.HasValue)
            {
                burnCount += count.Value;
            }
            else
            {
                burnCount++;
                if(destroyable != null)
                {
                    if(burnCount % 10 == 0)
                    {
                        destroyable.GetDamage(1);
                    }
                }
            }
            if (burnCount > hitsBeforeBurn)
            {
                if(freezeCounter != null && freezeCounter.frozen)
                {
                    freezeCounter.Melt();
                    burnCount = 0;
                }
                else
                {
                    BurnAction();
                }
            }
            lastBurn = Time.time;
        }
        else if(obj == null)
        {
            burn = false;
            burnCount = 0;
        }
    }

    protected virtual void BurnAction()
    {
        obj = Instantiate(flames, transform.position, flames.transform.rotation, transform);
        obj.transform.parent = transform;
        obj.transform.localScale = new Vector3(1, 1, 1);
        burn = true;
        if (burnableObject != null)
        {
            burnableObject.Burn(obj);
        }
        burnCount = 0;
    }

    public void StopBurning()
    {
        burn = false;
        burnCount = 0;
        if(burnableObject)
        {
            burnableObject.StopBurn();
        }
        if(obj)
        {
            obj.GetComponent<Flames>().Extinguish();
        }
    }
}
