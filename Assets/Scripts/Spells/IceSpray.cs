﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceSpray : ASpell
{
    public GameObject decalSystem;
    public GameObject hitSystem;

    private DecalManager decals;
    private ParticleSystem particleEmitter;
    private List<ParticleCollisionEvent> collisionEvents;
    private Transform cameraTransform;
    private ParticleSystem hitParticlesSystem;
    private float timer = 0;

    private void Start()
    {
        transform.position += transform.parent.forward * 0.5f;
        transform.Rotate(10, 0, 0);
        manaPool = FindObjectOfType<ManaPool>();
        if(manaPool != null && manaPool.GetManaAmount() <= manaUsage)
        {
            Destroy(this.gameObject);
        }
        if (!(decals = FindObjectOfType<DecalManager>()))
        {
            decals = Instantiate(decalSystem).GetComponent<DecalManager>();
        }
        else
        {
            ParticleSystem particleSystem = decals.GetComponent<ParticleSystem>();
            particleSystem.Play();
            var main = particleSystem.main;
            main.stopAction = ParticleSystemStopAction.None;
        }
        particleEmitter = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();
        cameraTransform = FindObjectOfType<Camera>().transform;
        hitParticlesSystem = hitSystem.GetComponent<ParticleSystem>();
    }

    private void Update()
    {
        timer += Time.deltaTime;
        if(manaPool != null && timer >= 1)
        {
            manaPool.DecreaseMana(manaUsage);
            if(manaPool.GetManaAmount() < manaUsage)
            {
                Destroy(this.gameObject);
            }
            timer = 0;
        }
    }

    public override void Cast()
    {
        Destroy(this.gameObject);
    }

    private void OnParticleCollision(GameObject other)
    {
        ParticlePhysicsExtensions.GetCollisionEvents(particleEmitter, other, collisionEvents);
        Vector3 normal;
        FreezeCounter freezeCounter = other.GetComponent<FreezeCounter>();
        if (freezeCounter != null)
        {
            freezeCounter.Freeze();
        }
        //else
        //{
        //    for (int i = 0; i < collisionEvents.Count; ++i)
        //    {
        //        normal = collisionEvents[i].normal;
        //        if (Vector3.Dot(normal, cameraTransform.forward) < 0)
        //        {
        //            normal = -normal;
        //        }
        //        decals.AddDecal(collisionEvents[i].intersection, normal);
        //    }
        //}
        hitSystem.transform.position = collisionEvents[0].intersection;
        hitSystem.transform.rotation = Quaternion.LookRotation(collisionEvents[0].normal);
        hitParticlesSystem.Emit(1);
        
        if(other.layer == LayerMask.NameToLayer("Ground"))
        {
            other.GetComponent<IcePainter>().Paint(collisionEvents[0].intersection);
        }
    }

    private void OnDestroy()
    {
        if (decals != null)
        {
            ParticleSystem particleSystem = decals.GetComponent<ParticleSystem>();
            particleSystem.Stop();
            var main = particleSystem.main;
            main.stopAction = ParticleSystemStopAction.Destroy;
        }
    }
}
