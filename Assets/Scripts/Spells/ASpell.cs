﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ASpell: MonoBehaviour
{
    public Sprite spellImage;
    public int manaUsage;
    protected ManaPool manaPool;

    public abstract void Cast();
}
