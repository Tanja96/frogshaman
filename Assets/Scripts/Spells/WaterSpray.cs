﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterSpray : ASpell
{
    private BoxCollider boxCollider;
    private FlyManager manager;
    private float timer = 0;

    public float maxDist;

    private void Start()
    {
        boxCollider = GetComponent<BoxCollider>();
        manager = FindObjectOfType<FlyManager>();
        transform.position += transform.parent.forward.normalized;
    }

    private void Update()
    {
        if(manager.elements[(int)FlyElement.water] == 0)
        {
            Destroy(gameObject);
        }
        timer += Time.deltaTime;
        if (timer > 0.5f)
        {
            manager.ChangeFlyCount(FlyElement.water, -1);
            timer = 0;
        }
        if (boxCollider.size.z < maxDist)
        {
            boxCollider.size += new Vector3(0, 0, 0.1f);
            boxCollider.center += new Vector3(0, 0, 0.05f);
        }
    }

    public override void Cast()
    {
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == LayerMask.NameToLayer("Enemy"))
        {
            other.GetComponent<Rigidbody>().AddForce(transform.forward * 10, ForceMode.Impulse);
        }
    }
}
