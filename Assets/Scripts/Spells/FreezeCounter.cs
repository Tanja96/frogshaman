﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezeCounter : MonoBehaviour
{
    private IceShard shard;
    private int freezeCount;
    private float lastFreeze;
    private float slowDownCurrent = 0;
    private ISpellEffect effect;
    private BurnCounter burnCounter;

    protected bool slowDown;
    public bool frozen;

    public GameObject iceShard;
    public int hitsBeforeFreeze;
    public float slowDownTime;
    public float slowDownSpeed;

    private void Start()
    {
        effect = GetComponent<Enemy>();
        burnCounter = GetComponent<BurnCounter>();
    }

    private void Update()
    {
        if (slowDown)
        {
            slowDownCurrent += Time.deltaTime;
            if (slowDownCurrent >= slowDownTime)
            {
                slowDown = false;
                effect.SetSpeed(true);
            }
        }
    }
    
    public void Freeze()
    {
        if (!frozen)
        {
            SlowDownAction();
            if (Time.time - lastFreeze <= 1)
            {
                freezeCount++;
                if (freezeCount > hitsBeforeFreeze)
                {
                    if(burnCounter != null && burnCounter.burn)
                    {
                        burnCounter.StopBurning();
                        freezeCount = 0;
                    }
                    else
                    {
                        FreezeAction();
                    }
                }
            }
            else
            {
                freezeCount = 0;
            }
            lastFreeze = Time.time;
        }
        if (shard != null)
        {
            shard.ResetTimer();
        }
    }

    protected virtual void SlowDownAction()
    {
        slowDown = true;
        effect.SetSpeed(false);
        slowDownCurrent = 0;
    }

    protected virtual void FreezeAction()
    {
        shard = Instantiate(iceShard, transform.position, iceShard.transform.rotation, transform).GetComponent<IceShard>();
        effect.SetStopped(true);
        frozen = true;
    }

    public void Melt()
    {
        frozen = false;
        effect.SetStopped(false);
        freezeCount = 0;
        if(shard)
        {
            Destroy(shard.gameObject);
        }
    }
}
