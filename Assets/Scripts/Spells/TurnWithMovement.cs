﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnWithMovement : MonoBehaviour
{
    private Vector3 lastPos;
    private Vector3 lastRot;
    public ParticleSystem[] systems;

    private ParticleSystem.ForceOverLifetimeModule[] modules;
    public float maxMovementTurn;
    public float maxRotationTurn;
    public float maxMovementx;
    public float maxMovementy;
    public float maxAngle;

    private void Start()
    {
        lastPos = transform.position;
        lastRot = transform.eulerAngles;
        modules = new ParticleSystem.ForceOverLifetimeModule[systems.Length];
        for(int i = 0; i < systems.Length; ++i)
        {
            modules[i] = systems[i].forceOverLifetime;
        }
    }

    private void Update()
    {
        Vector3 movement = transform.position - lastPos;
        movement = transform.InverseTransformDirection(movement);

        float xRot = transform.eulerAngles.y - lastRot.y;
        xRot = Mathf.Lerp(-maxRotationTurn, maxRotationTurn, Mathf.InverseLerp(-maxAngle, maxAngle, xRot));

        float x = Mathf.Lerp(-maxMovementTurn, maxMovementTurn, Mathf.InverseLerp(-maxMovementx, maxMovementx, movement.x));
        float y = Mathf.Lerp(-maxMovementTurn, maxMovementTurn, Mathf.InverseLerp(-maxMovementy, maxMovementy, movement.y));

        for (int i = 0; i < modules.Length; ++i)
        {
            modules[i].xMultiplier = - (x + xRot);
            modules[i].yMultiplier = - y;
        }
        lastPos = transform.position;
        lastRot = transform.eulerAngles;
    }
}
