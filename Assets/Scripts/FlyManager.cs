﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlyManager : MonoBehaviour {
    public int[] elements = new int[6];
    public Text[] texts = new Text[6];
    public int totalFlies;
    public int maxFlies = 100;

    public LayerMask collectLayer;
    public bool channeling;

    private void Start()
    {
        elements = new int[6];
    }

    private void Update()
    {
        if (channeling)
        {
            Collider[] flies = Physics.OverlapSphere(transform.position, 2.0f, collectLayer);
            if (flies.Length != 0)
            {
                foreach (Collider fly in flies)
                {
                    addFlies(fly);
                }
            }
        }
    }

    public void addFlies(Collider fly)
    {
        if (totalFlies < maxFlies)
        {
            FlyElement flyType = fly.GetComponent<Flies>().element;
            ++elements[(int)flyType];
            ++totalFlies;
            texts[(int)flyType].text = flyType.ToString() + ": " + elements[(int)flyType];
            Destroy(fly.transform.gameObject);
        }
    }

    public void ChangeFlyCount(FlyElement element, int amount)
    {
        elements[(int)element] += amount;
        totalFlies += amount;
        texts[(int)element].text = element.ToString() + ": " + elements[(int)element];
    }

    public void setChanneling(bool press)
    {
        channeling = press;
    }
    public bool getChanneling()
    {
        return channeling;
    }
}
