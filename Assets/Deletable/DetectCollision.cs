﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectCollision : MonoBehaviour
{
    public GameObject decalSystem;
    public GameObject hitSystem;

    private DecalManager decals;
    private ParticleSystem particleEmitter;
    private List<ParticleCollisionEvent> collisionEvents;
    private Transform cameraTransform;
    private ParticleSystem hitParticlesSystem;

    private void Start()
    {
        if(!(decals = FindObjectOfType<DecalManager>()))
        {
            decals = Instantiate(decalSystem).GetComponent<DecalManager>();
        }
        else
        {
            ParticleSystem particleSystem = decals.GetComponent<ParticleSystem>();
            particleSystem.Play();
            var main = particleSystem.main;
            main.stopAction = ParticleSystemStopAction.None;
        }
        particleEmitter = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();
        cameraTransform = FindObjectOfType<Camera>().transform;
        hitParticlesSystem = hitSystem.GetComponent<ParticleSystem>();
    }

    private void OnParticleCollision(GameObject other)
    {
        ParticlePhysicsExtensions.GetCollisionEvents(particleEmitter, other, collisionEvents);
        Vector3 normal;
        FreezeCounter freezable = other.GetComponent<FreezeCounter>();
        if (freezable != null)
        {
            freezable.Freeze();
        }
        else
        {
            for (int i = 0; i < collisionEvents.Count; ++i)
            {
                normal = collisionEvents[i].normal;
                if(Vector3.Dot(normal, cameraTransform.forward) < 0)
                {
                    normal = -normal;
                }
                decals.AddDecal(collisionEvents[i].intersection, normal);
            }
        }
        hitSystem.transform.position = collisionEvents[0].intersection;
        hitSystem.transform.rotation = Quaternion.LookRotation(collisionEvents[0].normal);
        hitParticlesSystem.Emit(1);
    }

    private void OnDestroy()
    {
        if(decals != null)
        {
            ParticleSystem particleSystem = decals.GetComponent<ParticleSystem>();
            particleSystem.Stop();
            var main = particleSystem.main;
            main.stopAction = ParticleSystemStopAction.Destroy;
        }
    }
}
