﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ColorVertices : MonoBehaviour
{
    public bool change;

    private void Update()
    {
        if (change == true)
        {
            Color[] c = new Color[] { Color.red, Color.green, Color.blue };
            Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
            int count = mesh.vertexCount;
            Color32[] colors = new Color32[count];
            int index = 0;
            for(int i = 0; i < count; ++i)
            {
                colors[i] = c[i % 3];
                index++;
            }
            mesh.colors32 = colors;
            change = false;
        }
    }
}
