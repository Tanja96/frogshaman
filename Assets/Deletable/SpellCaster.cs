﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellCaster : MonoBehaviour
{
    private ASpell spell;
    private Camera camera;

    public GameObject currentSpell;
    public Transform spellPosition;

    private void Start()
    {
        camera = GetComponent<Camera>();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Attack"))
        {
            //RaycastHit hit;
            //if(Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition), out hit))
            //{
            //    spellPosition.position = hit.point + Vector3.up;
            //}
            spell = Instantiate(currentSpell, spellPosition.position, spellPosition.rotation, spellPosition).GetComponent<ASpell>();
        }
        else if (Input.GetButtonUp("Attack"))
        {
            spell.Cast();
        }
    }
}
