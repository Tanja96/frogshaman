﻿Shader "Custom/Lightning"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_NoiseTex ("Noise Texture", 2D) = "white" {}
		_Magnitude("Magnitude", Range(0, 0.5)) = 0.05
		_Scale("Scale", Range(0, 2)) = 1
		_SpeedX ("Scrollspeed X", float) = 1
		_SpeedY ("Scrollspeed Y", float) = 1
		_ScrollSpeed("MainTex Scrollspeed", float) = 1
		_DissolveNoiseMap("Dissolve noise map", 2D) = "black" {}
		_EdgeSize("Edge size", Range(0,1)) = 0.4
		_Cutoff("Cutoff", Range(0,1)) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue"="Transparent" "IgnoreProjector" = "True" "PreviewType" = "Plane" }
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Off 
		Lighting Off 
		ZWrite Off
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				fixed4 color : COLOR;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
				fixed4 color : COLOR;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			sampler2D _NoiseTex;
			float _Period;
			float _Magnitude;
			float _Scale;
			float _SpeedX;
			float _SpeedY;
			float _ScrollSpeed;
			sampler2D _DissolveNoiseMap;
			fixed _EdgeSize;
			fixed _Cutoff;

            v2f vert (appdata v)
            {
                v2f o;
				o.color = v.color;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				float timeX = _Time[3] * _SpeedX;
				float timeY = _Time[3] * _SpeedY;
				float distX = tex2D(_NoiseTex, i.uv / _Scale + float2(timeX, 0)).r - 0.5;
				float distY = tex2D(_NoiseTex, i.uv / _Scale + float2(0, timeY)).r - 0.5;

				float2 distortion = float2(distX, distY);

                fixed4 col = tex2D(_MainTex, i.uv - float2(_Time[1] * _ScrollSpeed, 0) + distortion * _Magnitude) * i.color;

                UNITY_APPLY_FOG(i.fogCoord, col);

				half3 dissolveNoise = tex2D(_DissolveNoiseMap, i.uv);
				dissolveNoise.r = lerp(0, 1, dissolveNoise.r);
				_Cutoff = lerp(0, _Cutoff + _EdgeSize, _Cutoff);
				half edge = smoothstep(_Cutoff + _EdgeSize, _Cutoff, clamp(dissolveNoise.r, _EdgeSize, 1));

				clip(dissolveNoise - _Cutoff);
                return col;
            }
            ENDCG
        }
    }
}
