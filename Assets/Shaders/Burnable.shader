﻿Shader "Custom/Burnable"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
		[NoScaleOffset] _NormalMap ("Normal map", 2D) = "bump" {}
		_Smoothness("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0

		_EmissionMap ("Emission map", 2D) = "black" {}
		_EmissionCutOff ("Emission cutoff", Range(0,1)) = 0.5
		_EmissionColor ("Emission color", Color) = (0,0,0,0)
		[PerRendererData]_EmissionAmount ("Emission amount", Range(0,1)) = 0.5
		_EmissionStrengthMap ("Emission strength map", 2D) = "black" {}
		_ScrollSpeedX ("Scroll SpeedX", Float) = 1
		_ScrollSpeedY("Scroll SpeedY", Float) = 1

		[NoScaleOffset] _BurnedTexture ("Burned Texture", 2D) = "black" {}
		_BurnedColor ("Burned Color", Color) = (0,0,0,0)
		[PerRendererData]_BurnAmount ("BurnAmount", Range(0,1)) = 0.5

		_DissolveNoiseMap ("Dissolve noise map", 2D) = "black" {}
		_EdgeSize ("Edge size", Range(0,1)) = 0.4
		[PerRendererData]_Cutoff ("Cutoff", Range(0,1)) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        #pragma surface surf Standard fullforwardshadows addshadow
        #pragma target 3.0

        sampler2D _MainTex;
		sampler2D _EmissionStrengthMap;
		sampler2D _EmissionMap;
		sampler2D _DissolveNoiseMap;

        struct Input
        {
            float2 uv_MainTex;
			float2 uv_EmissionStrengthMap;
			float2 uv_EmissionMap;
			float2 uv_DissolveNoiseMap;
        };

		fixed _Smoothness;
		fixed _Metallic;
        fixed4 _Color;
		fixed _EmissionCutOff;
		fixed4 _EmissionColor;
		sampler2D _NormalMap;
		fixed _ScrollSpeedX;
		fixed _ScrollSpeedY;
		sampler2D _BurnedTexture;
		fixed4 _BurnedColor;
		fixed _EdgeSize;

        // #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			UNITY_DEFINE_INSTANCED_PROP(fixed, _BurnAmount)
			UNITY_DEFINE_INSTANCED_PROP(fixed, _EmissionAmount)
			UNITY_DEFINE_INSTANCED_PROP(fixed, _Cutoff)
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
			fixed burnAmount = UNITY_ACCESS_INSTANCED_PROP(Props, _BurnAmount);
			fixed emissionAmount = UNITY_ACCESS_INSTANCED_PROP(Props, _EmissionAmount);
			fixed cutoff = UNITY_ACCESS_INSTANCED_PROP(Props, _Cutoff);

			//Lerping between burned and normal color by burnAmount that comes from script
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			fixed4 c2 = tex2D (_BurnedTexture, IN.uv_MainTex) * _Color;
            o.Albedo = lerp(c.rgb, c2.rgb, burnAmount);

			o.Normal = UnpackNormal(tex2D(_NormalMap, IN.uv_MainTex));
			o.Alpha = c.a;
			o.Smoothness = _Smoothness;
			o.Metallic = _Metallic;

			if (emissionAmount > 0 || cutoff > 0)
			{
				//Getting emission strength movement from moving the strengthmap and emission strength map value
				fixed scrollX = _Time * _ScrollSpeedX;
				fixed scrollY = _Time * _ScrollSpeedY;
				fixed4 noise = tex2D(_EmissionStrengthMap, fixed2(scrollX, scrollY));
				fixed4 noise2 = tex2D(_EmissionStrengthMap, IN.uv_EmissionStrengthMap);

				//Calculating if the current pixel is on a dissolve edge
				half3 dissolveNoise = tex2D(_DissolveNoiseMap, IN.uv_DissolveNoiseMap);
				dissolveNoise.r = lerp(0, 1, dissolveNoise.r);
				cutoff = lerp(0, cutoff + _EdgeSize, cutoff);
				half edge = smoothstep(cutoff + _EdgeSize, cutoff, clamp(dissolveNoise.r, _EdgeSize, 1));

				//Getting emission Color and place of emission and multiplying with emission strength values. Later adding edge to the emission amount
				fixed4 em = tex2D(_EmissionMap, IN.uv_EmissionMap);
				fixed4 emissionColor;
				if (em.a > _EmissionCutOff)
				{
					emissionColor = _EmissionColor * emissionAmount * noise2.a * noise.a;
				}
				else
				{
					emissionColor = 0;
				}
				o.Emission = emissionColor + _EmissionColor * edge;

				//Removes the pixel before output if the value is negative
				clip(dissolveNoise - cutoff);
			}
        }
        ENDCG
    }
    FallBack "Diffuse"
}
