﻿Shader "Custom/Ice"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_Color ("Edge color", COLOR) = (0,0,0,0)
		_EdgeWidth ("Width of the edge", Float) = 0.1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				fixed4 color : COLOR;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				fixed4 tri : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			fixed4 _Color;
			fixed _EdgeWidth;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.tri = v.color;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 fill_color = tex2D(_MainTex, i.uv);
				float3 d = fwidth(i.tri);
				float3 tdist = smoothstep(float3(0, 0, 0), d * _EdgeWidth, i.tri);
				return lerp(_Color, fill_color, min(min(tdist.x, tdist.y), tdist.z));
            }
            ENDCG
        }
    }
}
