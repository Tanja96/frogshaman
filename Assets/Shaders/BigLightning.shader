﻿Shader "Custom/BigLightning"
{
    Properties
    {
		[Toggle]_Emission("Emission?", Float) = 0
        _EmissionColor ("Emission Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
    }
    SubShader
    {
		Tags {"Queue" = "Transparent" "RenderType" = "Transparent""IgnoreProjector" = "True" "PreviewType" = "Plane"}
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Off
		Lighting Off
		ZWrite Off
		LOD 200

        CGPROGRAM
		#pragma surface surf Lambert alpha vertex:vert
        #pragma target 3.0

        sampler2D _MainTex;

		struct appdata_particles
		{
			float4 vertex : POSITION;
			float3 normal : NORMAL;
			float4 color : COLOR;
			float4 texcoord : TEXCOORD0;
		};

		struct Input
		{
			float2 uv_MainTex;
			float4 color;
			float revealAmount;
		};

        fixed4 _EmissionColor;
		fixed _Emission;

        UNITY_INSTANCING_BUFFER_START(Props)
        UNITY_INSTANCING_BUFFER_END(Props)

		void vert(inout appdata_particles v, out Input o)
		{
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.uv_MainTex = v.texcoord.xy;
			o.revealAmount = v.texcoord.z;
			o.color = v.color;
		}

        void surf (Input IN, inout SurfaceOutput o)
        {
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * IN.color;
			if (_Emission == 1)
			{
				o.Emission = _EmissionColor;
			}
            o.Albedo = c.rgb;
            o.Alpha = c.a;

			if (IN.revealAmount != 1)
			{
				clip(IN.revealAmount - IN.uv_MainTex.x);
			}
        }
        ENDCG
    }
    FallBack "Diffuse"
}
