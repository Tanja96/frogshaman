﻿Shader "Custom/GroundIce"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
		_IceTex ("Ice Texture", 2D) = "black" {}
		_IceColor ("Ice Color", Color) = (0,0,0,0)
		_MapTex ("Map Texture", 2D) = "black" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        #pragma surface surf Standard fullforwardshadows
        #pragma target 3.0

        sampler2D _MainTex;
		sampler2D _IceTex;
		sampler2D _MapTex;

        struct Input
        {
            float2 uv_MainTex;
			float2 uv_IceTex;
			float2 uv_MapTex;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;
		fixed4 _IceColor;

        UNITY_INSTANCING_BUFFER_START(Props)
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			fixed4 ice = tex2D(_IceTex, IN.uv_IceTex) * _IceColor;
			fixed4 map = tex2D(_MapTex, IN.uv_MapTex);
			c = lerp(c, ice, map.r);
            o.Albedo = c.rgb;
            o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
