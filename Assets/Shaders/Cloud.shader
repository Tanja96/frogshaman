﻿Shader "Custom/CloudParticle"
{
    Properties
    {
		_Color("Color", Color) = (1,1,1,1)
		[Toggle] _UseTextureColor("Use texture color?", Float) = 0
        _MainTex ("Texture", 2D) = "white" {}
		_ScrollSpeedX("ScrollSpeed X", Range(-10, 10)) = 1
		_ScrollSpeedY("ScrollSpeed Y ", Range(-10, 10)) = 1
		_OtherTex ("Other texture", 2D) = "white" {}
		_ScrollSpeed2X("Other scrollSpeed X", Range(-10, 10)) = 1
		_ScrollSpeed2Y("Other scrollSpeed Y", Range(-10, 10)) = 1
		_MaskTex ("Mask", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue" = "Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite Off
		Cull Back
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
				fixed4 color : COLOR;
                float2 uv : TEXCOORD0;
				float2 uv2 : TEXCOORD1;
				float2 uv3 : TEXCOORD2;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
				float2 uv2 : TEXCOORD1;
				float2 uv3 : TEXCOORD2;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
            };

			fixed4 _Color;
            sampler2D _MainTex;
            float4 _MainTex_ST;
			sampler2D _OtherTex;
			float4 _OtherTex_ST;
			sampler2D _MaskTex;
			float4 _MaskTex_ST;
			float _ScrollSpeedX;
			float _ScrollSpeed2X;
			float _ScrollSpeedY;
			float _ScrollSpeed2Y;
			fixed _UseTextureColor;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.uv2 = TRANSFORM_TEX(v.uv2, _OtherTex);
				o.uv3 = TRANSFORM_TEX(v.uv3, _MaskTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
				o.color = v.color;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
				fixed4 mask = tex2D(_MaskTex, i.uv3);
				float scrollX = _Time * _ScrollSpeedX;
				float scrollY = _Time * _ScrollSpeedY;
				float scroll2X = _Time * _ScrollSpeed2X;
				float scroll2Y = _Time * _ScrollSpeed2Y;
				fixed4 main = tex2D(_MainTex, i.uv + float2(scrollX, scrollY));
				fixed4 other = tex2D(_OtherTex, i.uv2 + float2(scroll2X, scroll2Y));
				fixed4 col = _Color * i.color;
				if (_UseTextureColor == 1)
				{
					col = col * main;
				}
				col.a = main.r * other.r * mask.r * i.color.a;
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
