﻿Shader "Custom/LavaBomb"
{
    Properties
    {
		_MainTex("Texture", 2D) = "white" {}
		_EmissionMask("Emission mask", 2D) = "white" {}
		_EmissionTexture("EmissionTex", 2D) = "white" {}
		_EmissionColor("Color", Color) = (1,1,1,1)
		_NormalMap("Normal map", 2D) = "bump" {}
		_Amount ("Amount", Range(0,1)) = 0.5
		[PerRendererData]_Speed ("Speed", Range(0,10)) = 0
		_IntensityMin ("Emission intensity min", Range(0,3)) = 1.2
		_IntensityMax ("Emission intensity max", Range(1,4)) = 2
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        #pragma surface surf Lambert vertex:vert addshadow

        #pragma target 3.0

        sampler2D _MainTex;
		sampler2D _EmissionMask;
		sampler2D _EmissionTexture;
		sampler2D _NormalMap;
		float4 _EmissionColor;
		float _Amount;
		float _IntensityMin;
		float _IntensityMax;

        struct Input
        {
            float2 uv_MainTex;
			float2 uv_EmissionMask;
			float2 uv_EmissionTexture;
        };

        UNITY_INSTANCING_BUFFER_START(Props)
			UNITY_DEFINE_INSTANCED_PROP(float, _Speed)
        UNITY_INSTANCING_BUFFER_END(Props)

		void vert(inout appdata_full v)
		{
			float time = sin(UNITY_ACCESS_INSTANCED_PROP(Props, _Speed)) * _Amount * 0.1;
			v.vertex.xyz += v.normal * time;
		}

        void surf (Input IN, inout SurfaceOutput o)
        {
			fixed4 main = tex2D(_MainTex, IN.uv_MainTex);
			float intensity = _IntensityMin;
			float speed = UNITY_ACCESS_INSTANCED_PROP(Props, _Speed);
			if (speed != 0)
			{
				float sinMultiplier = (_IntensityMax - _IntensityMin) * 0.5;
				intensity = sin(speed) * sinMultiplier + sinMultiplier + _IntensityMin;
			}
			fixed4 emission = tex2D(_EmissionTexture, IN.uv_EmissionTexture) * _EmissionColor * intensity;
			fixed4 c = lerp(main, emission, tex2D(_EmissionMask, IN.uv_EmissionMask).r);
            o.Albedo = c.rgb;
            o.Alpha = c.a;
			o.Normal = UnpackNormal(tex2D(_NormalMap, IN.uv_MainTex));
        }
        ENDCG
    }
}
