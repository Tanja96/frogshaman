﻿Shader "Custom/DrawShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "black" {}
		_Coordinate("Coordinate", Vector) = (0,0,0,0)
		_DrawWidth("Draw width", Float) = 50
		_Color("Color", Color) = (0,0,0,0)
		_Strength ("Strength", Range(0,1)) = 1
		_Dir("Direction", Float) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			fixed4 _Coordinate;
			fixed4 _Color;
			float _DrawWidth;
			float _Strength;
			float _Dir;

            v2f vert (appdata v)
            {
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				//Coming from Graphics.Blit() source
				fixed4 originalColor = tex2D(_MainTex, i.uv);
				float draw = pow(saturate(1 - distance(i.uv, _Coordinate.xy)), _DrawWidth);
				fixed4 drawColor = _Color * (draw * _Strength);
				return saturate(originalColor + _Dir * drawColor);
            }
            ENDCG
        }
    }
}
