﻿Shader "Custom/EmissiveLightning"
{
    Properties
    {
        _MainTex ("Main Texture", 2D) = "white" {}
        [HDR]_EmissionColor ("Emission Color", Color) = (1,1,1,1)
		_NoiseTex("Noise Texture", 2D) = "white" {}
		_Magnitude("Magnitude", Range(0, 0.2)) = 0.05
		_Scale("Scale", Range(0, 3)) = 1
		_SpeedX("Scrollspeed X", float) = 1
		_SpeedY("Scrollspeed Y", float) = 1
		_ScrollSpeed("MainTex Scrollspeed", float) = 1
    }
    SubShader
    {
		Tags {"Queue" = "Transparent" "RenderType" = "Transparent""IgnoreProjector" = "True" "PreviewType" = "Plane"}
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Off
		Lighting Off
		ZWrite Off
		LOD 200

        CGPROGRAM
		#pragma surface surf Lambert alpha vertex:vert
        #pragma target 3.0

        sampler2D _MainTex;

		struct appdata_particles
		{
			float4 vertex : POSITION;
			float3 normal : NORMAL;
			float4 color : COLOR;
			float4 texcoord : TEXCOORD0;
		};

        struct Input
        {
            float2 uv_MainTex;
			float4 color;
        };

        fixed4 _EmissionColor;
		sampler2D _NoiseTex;
		float _Period;
		float _Magnitude;
		float _Scale;
		float _SpeedX;
		float _SpeedY;
		float _ScrollSpeed;

        UNITY_INSTANCING_BUFFER_START(Props)
        UNITY_INSTANCING_BUFFER_END(Props)

		void vert(inout appdata_particles v, out Input o)
		{
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.uv_MainTex = v.texcoord.xy;
			o.color = v.color;
		}

        void surf (Input IN, inout SurfaceOutput o)
        {
			float timeX = _Time[3] * _SpeedX;
			float timeY = _Time[3] * _SpeedY;
			float distX = tex2D(_NoiseTex, IN.uv_MainTex / _Scale + float2(timeX, 0)).r - 0.5;
			float distY = tex2D(_NoiseTex, IN.uv_MainTex / _Scale + float2(0, timeY)).r - 0.5;

			float2 distortion = float2(distX, distY);

			fixed4 c = tex2D(_MainTex, IN.uv_MainTex - float2(_Time[1] * _ScrollSpeed, 0) + distortion * _Magnitude) * IN.color;
            o.Albedo = c.rgb;
            o.Alpha = c.a;
			o.Emission = _EmissionColor;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
